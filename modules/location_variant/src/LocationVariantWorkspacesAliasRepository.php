<?php

namespace Drupal\location_variant;

use Drupal\workspaces\WorkspacesAliasRepository;

/**
 * Provides workspace-specific path alias lookup queries that are variant aware.
 */
class LocationVariantWorkspacesAliasRepository extends WorkspacesAliasRepository {

  use LocationVariantAliasRepositoryTrait;

}
