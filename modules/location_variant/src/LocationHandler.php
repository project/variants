<?php

namespace Drupal\location_variant;

use Drupal\location_variant\Event\LocationsEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Location handler.
 */
class LocationHandler {

  const DEFAULT_LOCATION = 'global';
  const DEFAULT_LANGUAGE = 'en';
  const DEFAULT_VARIATION_CODE = 'en-0';

  /**
   * @var \Drupal\location_variant\Location[]
   */
  protected array $locationHierarchy = [];


  /**
   * @var \Drupal\location_variant\Location[]
   */
  protected array $locations = [];

  protected EventDispatcherInterface $dispatcher;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected RequestStack $requestStack;

  public function __construct(EventDispatcherInterface $dispatcher, EntityTypeManagerInterface $entityTypeManager, RequestStack $requestStack) {
    $this->dispatcher = $dispatcher;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * Get locations.
   *
   * @param bool $rebuild
   *   Flag to rebuild locations.
   *
   * @return \Drupal\location_variant\Location[]
   *   An array of locations.
   */
  public function getLocations(bool $rebuild = FALSE): array {
    if ($this->locations && !$rebuild) {
      return $this->locations;
    }
    $event = new LocationsEvent();
    $this->dispatcher->dispatch($event);
    $this->locations = $event->getLocations();
    return $this->locations;
  }

  /**
   * Get location object.
   *
   * @param string $location_id
   *   The location ID.
   *
   * @return \Drupal\location_variant\Location|null
   *   The location.
   */
  public function getLocation(string $location_id): ?Location {
    $locations = $this->getLocations();
    return $locations[$location_id] ?? NULL;
  }

  /**
   * Get location lineage.
   *
   * For example: Austin > US > global.
   *
   * @param string $location_id
   *   The location ID.
   * @param bool $recurring
   *   Flags whether we are recurring.
   *
   * @return array
   *   An array of location IDs.
   */
  public function getLocationLineage(string $location_id, bool $recurring = FALSE): array {
    static $lineage;
    if (\is_array($lineage) && !$recurring) {
      if (\in_array($location_id, $lineage)) {
        return $lineage;
      }
      else {
        $lineage = [];
      }
    }

    $lineage[] = $location_id;
    $parent = $this->getLocation($location_id)->getParent();
    if ($parent) {
      $this->getLocationLineage($parent->getId(), TRUE);
    }
    return $lineage;
  }

  /**
   * Get an array of location options.
   *
   * @param bool $rebuild
   *   Flag to rebuild locations.
   *
   * @return array
   *   An array of location options.
   */
  public function getLocationOptions(bool $rebuild = FALSE): array {
    $options = [];
    $tree = $this->getLocationHierarchy($rebuild);
    foreach ($tree as $location) {
      $options[$location->getId()] = $location->getLabel();
      $this->appendChildOptions($location, $options);
    }

    unset($options[self::DEFAULT_LOCATION]);

    return $options;
  }

  /**
   * Append child options.
   *
   * Recursively add children From the location hierarchy.
   *
   * @param \Drupal\location_variant\Location $location
   *   The location.
   * @param array $options
   *   The location options.
   * @param int $depth
   *   The depth of recursion.
   */
  public function appendChildOptions(Location $location, array &$options, $depth = 0) {
    $children = $location->getChildren();
    if ($children) {
      foreach ($children as $child) {
        $indentation = '';
        for ($i = 0; $i < $depth; $i++) {
          $indentation .= "-";
        }
        $options[$child->getId()] = " $indentation {$child->getLabel()}";
        $this->appendChildOptions($child, $options, $depth + 1);
      }
    }
  }

  /**
   * Get location hierarchy.
   *
   * @param bool $rebuild
   *   Flag to rebuild locations.
   *
   * @return array
   *   A tree of locations.
   */
  public function getLocationHierarchy(bool $rebuild = FALSE): array {
    $locations = $this->getLocations($rebuild);
    if (!$this->locationHierarchy) {
      $tree = [];
      foreach ($locations as $location) {
        if ($parent = $location->getParent()) {
          $locations[$parent->getId()]->addChild($location);
        }
        else {
          $tree[$location->getId()] = $location;
        }
      }
      $this->locationHierarchy = $tree;
    }
    return $this->locationHierarchy;
  }

  /**
   * Get supported langcodes that a location supports.
   *
   * @param \Drupal\location_variant\Location $location
   *   The location.
   *
   * @return array
   *   An array of supported langcodes codes.
   */
  public function getSupportedLangcodes(Location $location): array {
    $supported_langcodes = [];
    foreach ($location->getLanguages() as $langcode => $info) {
      $supported_langcodes[] = $this->generateLangcode($location->getId(), $langcode);
    }
    return $supported_langcodes;
  }

  /**
   * Generate langcode.
   *
   * The translation system is used for both localizations and translations. The
   * langcodes are like unique IDs. These are the langcodes that are used as
   * URL prefixes. e.g. en-US, de-DE, ko-KR.
   *
   * @param string $location
   *   The supported locations within the Location Reference.
   * @param string $langcode
   *   The supported langcode for a translation.
   *
   * @return string
   *   The langcode.
   */
  public function generateLangcode(string $location, string $langcode): string {
    return "$langcode-$location";
  }

  /**
   * Parse Langcode.
   *
   * @param string $langcode
   *   The langcode as it is used in URL prefixes.
   *
   * @return array
   *   The langcode parsed into the langcode and either the location or variant
   *   delta.
   */
  public static function parseCode(string $langcode): array {
    $pieces = \explode('-', $langcode);
    $location_or_delta = \array_pop($pieces);
    $info['type'] = \is_numeric($location_or_delta) ? 'variation_code' : 'langcode';
    $info['location'] = $location_or_delta;
    $info['langcode'] = \implode('-', $pieces);
    $info['variation_type'] = 'variation';
    if ($info['type'] === 'langcode') {
      if ($info['location'] === LocationHandler::DEFAULT_LOCATION) {
        $info['variation_type'] = $info['langcode'] === LocationHandler::DEFAULT_LANGUAGE ? 'default' : 'translation';
      }
      else {
        $info['variation_type'] = $info['langcode'] === LocationHandler::DEFAULT_LANGUAGE ? 'localization' : 'localized translation';
      }
    }

    return $info;
  }

  /**
   * Is Variation code.
   *
   * @param string $code
   *   The variation code or langcode to check.
   *
   * @return bool
   *   Whether the code is a variation code.
   */
  public static function isVariationCode(string $code): bool {
    $code_info = self::parseCode($code);
    return $code_info['type'] === 'variation_code';
  }

}
