<?php

namespace Drupal\location_variant\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\location_variant\LocationHandler;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\location_variant\EntityVariationHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Location Variant routes.
 */
class DynamicallyCreateVariation extends ControllerBase {

  protected LocationHandler $locationHandler;
  protected EntityVariationHandler $variationHandler;
  protected EntityRepositoryInterface $entityRepository;

  public function __construct(EntityRepositoryInterface $entity_repository, LocationHandler $location_handler, EntityVariationHandler $entity_handler) {
    $this->locationHandler = $location_handler;
    $this->variationHandler = $entity_handler;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('location_variant.location_handler'),
      $container->get('location_variant.entity_handler')
    );
  }

  /**
   * Access callback.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   Whether the user can create this entity type.
   */
  public function access(RouteMatchInterface $route_match) {
    $entity_type_id = $route_match->getParameter('entity_type_id');
    $entity_id = $route_match->getParameter('id');
    $entity = $this->entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    return AccessResult::allowedIf($entity->access('create'));
  }

  /**
   * Build.
   *
   * Redirects users to an entity form to create a localization, translation, or
   * localized translation.
   */
  public function build(Request $request) {
    $entity_type_id = $request->get('entity_type_id');
    $entity_id = $request->get('id');
    $entity = $this->entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    $current_language = $this->languageManager()->getCurrentLanguage()->getId();
    $langcode_info = LocationHandler::parseCode($current_language);
    $variations = $this->variationHandler->getVariations($entity);
    $response = NULL;

    if (!empty($variations[$current_language]) && $variations[$current_language]['exists']) {
      // If some how a user gets to this controller and the variation already
      // exists, lets just forward them along to the correct edit form.
      switch ($langcode_info['variation_type']) {
        case 'localization':
          $response = $variations[$current_language]['variation']->toUrl('localization.edit')->toString();
          break;

        case 'localized translation':
        case 'translation':
          $response = $variations[$current_language]['variation']->toUrl('translation.edit')->toString();
          break;

        default:
          $response = $variations[$current_language]['variation']->toUrl('edit-form')->toString();
      }
    }
    else {
      // Redirect users to the correct entity form with the required parameters
      // populated.
      $closest_translation = $this->entityRepository->getTranslationFromContext($entity, $current_language);
      switch ($langcode_info['variation_type']) {
        case 'translation':
          $response = $this->variationHandler->getVariationAddUrl($variations[$current_language]['variation'], $variations[$current_language]['variation code'], $current_language, FALSE)->toString();
          break;

        case 'localization':
          $location = $this->locationHandler->getLocation($langcode_info['location']);

          $response = Url::fromRoute("entity.$entity_type_id.localization.add", [
            $entity_type_id => $entity_id,
            'source' => $closest_translation->language()->getId(),
            'target' => $this->variationHandler->generateVariationCode($langcode_info['langcode'], $entity),
          ], [
            'query' => [
              'locations' => [$location->getId()],
            ],
          ])->toString();
          break;

        case 'localized translation':
          $location = $this->locationHandler->getLocation($langcode_info['location']);

          $response = Url::fromRoute("entity.$entity_type_id.translation.add", [
            $entity_type_id => $entity_id,
            'source' => $closest_translation->language()->getId(),
            'target' => $this->variationHandler->generateVariationCode($langcode_info['langcode'], $entity),
          ], [
            'query' => [
              'locations' => [$location->getId()],
            ],
          ])->toString();
          break;
      }
    }

    if ($response) {
      return new RedirectResponse($response, 302);
    }

    $this->messenger()->addWarning($this->t("I'm sorry, something went wrong. I couldn't find the correct form to redirect you to."));
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('If the problem persists please report it to the website administrator.'),
    ];

    return $build;
  }

}
