<?php

namespace Drupal\location_variant\Controller;

use Drupal\variants\VariantHandler;
use Drupal\Core\Controller\ControllerBase;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\location_variant\EntityVariationHandler;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Location Controller.
 */
class LocationController extends ControllerBase {

  private RedirectDestinationInterface $destination;
  private VariantHandler $variantHandler;
  private EntityVariationHandler $entityVariationHandler;

  public function __construct(RedirectDestinationInterface $destination, VariantHandler $variant_handler, EntityVariationHandler $entity_location_handler) {
    $this->destination = $destination;
    $this->variantHandler = $variant_handler;
    $this->entityVariationHandler = $entity_location_handler;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('redirect.destination'),
      $container->get('variant_handler'),
      $container->get('location_variant.entity_handler')
    );
  }

  /**
   * Builds the localizations overview page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   Array of page elements to render.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function overview(RouteMatchInterface $route_match, string $entity_type_id = NULL) {
    $entity = $route_match->getParameter($entity_type_id);

    $variations = $this->entityVariationHandler->getVariations($entity);
    $rows = $row_values = $row_deltas = [];
    // Aggregate the row info grouping by variant.
    foreach ($variations as $langcode => $variation) {
      $variation_code_info = LocationHandler::parseCode($variation['variation code']);
      $row_deltas[$row_delta] = $row_delta = $variation_code_info['location'];

      if ($variation['exists']) {
        $row_values['locations'][$row_delta] = $variation['variant']->getLocationIds();

        if (!empty($row_values['languages'][$row_delta])) {
          $row_values['languages'][$row_delta][] = ['#markup' => ', '];
        }
        $row_values['languages'][$row_delta][] = [
          '#type' => 'link',
          '#title' => $langcode,
          '#url' => $this->entityVariationHandler->getVariationCanonicalUrl($variation['variation'], $langcode),
        ];
        $row_values['titles'][$row_delta][$variation_code_info['langcode']] = $variation['variation']->label();

        $row_values['operations'][$row_delta]["edit_$langcode"] = [
          'url' => $this->entityVariationHandler->getVariationEditUrl($variation['variation'], $variation['variant'], $variation['variation code'], $langcode),
          'title' => $this->t('Edit @lang', ['@lang' => $langcode]),
        ];
      }
      else {
        $row_values['operations'][$row_delta]["add_$langcode"] = [
          'url' => $this->entityVariationHandler->getVariationAddUrl($variation['variation'], $variation['variation code'], $langcode),
          'title' => $this->t('Add @lang', ['@lang' => $langcode]),
        ];
      }
    }

    foreach ($row_deltas as $row_delta) {
      $rows[] = [
        'locations' => \implode('/', $row_values['locations'][$row_delta]),
        'languages' => ['data' => $row_values['languages'][$row_delta]],
        'title' => !empty($row_values['titles'][$row_delta][LocationHandler::DEFAULT_LANGUAGE]) ? $row_values['titles'][$row_delta][LocationHandler::DEFAULT_LANGUAGE] : $row_values['titles'][$row_delta][array_key_first($row_values['titles'][$row_delta])],
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => $row_values['operations'][$row_delta],
          ],
        ],
      ];
    }

    $build['localization_overview'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Location'),
        $this->t('Language Code'),
        $this->t('Title'),
        $this->t('Operations'),
      ],
      '#rows' => $rows,
      '#cache' => [
        'tags' => ["{$entity_type_id}:{$entity->id()}"],
      ],
    ];
    $build['#attached']['library'][] = 'location_variant/overview';

    return $build;
  }

  /**
   * Builds an add Localization page.
   *
   * @param string $source
   *   The language of the values being translated. Defaults to the entity
   *   language.
   * @param string $target
   *   The language of the translated values. Defaults to the current content
   *   language.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function add(string $source, string $target, RouteMatchInterface $route_match, $entity_type_id = NULL) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($entity_type_id);
    $this->variantHandler->prepareVariation($entity, $source, $target);
    $operation = $entity->getEntityType()->hasHandlerClass('form', 'add') ? 'add' : 'default';
    $form_state_additions['langcode'] = $target;

    return $this->entityFormBuilder()->getForm($entity, $operation, $form_state_additions);
  }

  /**
   * Builds the edit localization page.
   *
   * @param string $language
   *   The language of the translated values. Defaults to the current content
   *   language.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object from which to extract the entity type.
   * @param string $entity_type_id
   *   (optional) The entity type ID.
   *
   * @return array
   *   A processed form array ready to be rendered.
   */
  public function edit(string $language, RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity = $route_match->getParameter($entity_type_id);
    $operation = $entity->getEntityType()->hasHandlerClass('form', 'edit') ? 'edit' : 'default';
    $form_state_additions['langcode'] = $language;

    return $this->entityFormBuilder()->getForm($entity, $operation, $form_state_additions);
  }

}
