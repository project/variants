<?php

namespace Drupal\location_variant\Routing;

use Symfony\Component\Routing\Route;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Subscriber for entity translation routes.
 */
class LocationRouteSubscriber extends RouteSubscriberBase {

  private ImmutableConfig $config;
  private EntityTypeManagerInterface $entityTypeManager;

  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get('variants.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $enabled_entity_types = $this->config->get('entity_types');
    if ($enabled_entity_types) {
      foreach (array_keys($enabled_entity_types) as $entity_type_id) {
        // Inherit admin route status from edit route, if exists.
        $is_admin = FALSE;
        if ($edit_route = $collection->get("entity.$entity_type_id.edit_form")) {
          $is_admin = (bool) $edit_route->getOption('_admin_route');
        }
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        // Localizations overview route.
        $route = new Route(
          // We need to match the content translation link template name for our
          // overview page as it is hard coded into MenuLinkContent.
          $entity_type->getLinkTemplate('drupal:content-translation-overview'),
          [
            '_controller' => '\Drupal\location_variant\Controller\LocationController::overview',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.update',
            '_permission' => 'create and edit localizations',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.content_translation_overview", $route);

        // Add Location route.
        $route = new Route(
          $entity_type->getLinkTemplate('location.add'),
          [
            '_form' => '\Drupal\location_variant\Form\AddLocation',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.update',
            '_permission' => 'create and edit localizations',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.location.add", $route);

        // Add Localization route.
        $route = new Route(
          $entity_type->getLinkTemplate('localization.add'),
          [
            '_controller' => '\Drupal\location_variant\Controller\LocationController::add',
            'source' => NULL,
            'target' => NULL,
            '_title' => 'Add',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.update',
            '_permission' => 'create and edit localizations',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.localization.add", $route);

        // Edit Localization route.
        $route = new Route(
          $entity_type->getLinkTemplate('localization.edit'),
          [
            '_controller' => '\Drupal\location_variant\Controller\LocationController::edit',
            'language' => NULL,
            '_title' => 'Edit',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.update',
            '_permission' => 'create and edit localizations',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
                'load_latest_revision' => TRUE,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.localization.edit", $route);

        // Add Translation route.
        $route = new Route(
          $entity_type->getLinkTemplate('translation.add'),
          [
            '_controller' => '\Drupal\location_variant\Controller\LocationController::add',
            'source' => NULL,
            'target' => NULL,
            '_title' => 'Add',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.update',
            '_permission' => 'create and edit localizations',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.translation.add", $route);

        // Edit Translation route.
        $route = new Route(
          $entity_type->getLinkTemplate('translation.edit'),
          [
            '_controller' => '\Drupal\location_variant\Controller\LocationController::edit',
            'language' => NULL,
            '_title' => 'Edit',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.update',
            '_permission' => 'create and edit localizations',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
                'load_latest_revision' => TRUE,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.translation.edit", $route);
      }
    }
  }

}
