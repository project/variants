<?php

namespace Drupal\location_variant\Plugin\views\relationship;

use Drupal\Core\Form\FormStateInterface;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;

/**
 * Entity Variant Relationship.
 *
 * Relationship handler to relate the currently selected variation code (langcode
 * + location) to a variation. This is needed to filter a view down to the most
 * specific variation for the user and not just list all of the entities
 * variations.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("variant")
 */
class Variant extends RelationshipPluginBase {

  private LocationHandler $locationHandler;
  private LanguageManagerInterface $languageManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, LocationHandler $location_handler, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->locationHandler = $location_handler;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('location_variant.location_handler'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['required']);
  }

  /**
   * Add the variant relationship to the view.
   */
  public function query() {
    $def = $this->definition;
    $id_key = $def['id_key'];
    $field_data_table = $def['field_data'];
    $variant_reference = $def['base'];
    $location_field = 'variant__locations';
    $language = $this->languageManager->getCurrentLanguage()->getId();

    if (!\str_contains($language, '-')) {
      $this->invalidLangcode($language, $field_data_table, $id_key);
      return;
    }
    if (LocationHandler::isVariationCode($language)) {
      $this->invalidLangcode($language, $field_data_table, $id_key);
      return;
    }
    $info = LocationHandler::parseCode($language);
    $langcode = $info['langcode'];
    $location_code = $info['location'];

    $locations = $this->locationHandler->getLocationLineage($location_code);

    // Are we in a workspace?
    if ($this->moduleHandler->moduleExists('workspaces')) {
      $workspace_manager = \Drupal::service('workspaces.manager');
      if ($workspace_manager->hasactiveworkspace()) {
        $variant_reference = $def['base_revision'];
        $location_field = 'variant_revision__locations';
      }
    }

    // Normally with a variation code like ko-2 you would extract the ko part
    // with SUBSTRING_INDEX(langcode, '-', 1). We need to be able to extract the
    // langcode zh-hant from zh-hant-0 as well though. Lets flip the string over
    // extract the part we want and then flip it back around.
    $extract_language_from_variation_code = "REVERSE(SUBSTRING(REVERSE(fdt.langcode), LOCATE('-', fdt.langcode), LENGTH(fdt.langcode)))";

    // Sort the results by location.
    $depth = 0;
    $cases = '';
    foreach ($locations as $location_id) {
      // Dividing the depth means that you get a score of 1, .5, .25, .12,
      // and so on as depth increases.
      $weight = 1 / ++$depth;
      $cases .= "\n  WHEN vl.locations_value = '$location_id' THEN $weight ";
    }
    $order_by = "ORDER BY CASE $cases \nEND DESC,\n";

    // Sort the results by language preference.
    $cases = '';
    if ($langcode !== LocationHandler::DEFAULT_LANGUAGE) {
      $cases .= "\n  WHEN $extract_language_from_variation_code = '$langcode' THEN 1 ";
    }
    $cases .= "\n  WHEN $extract_language_from_variation_code = '" . LocationHandler::DEFAULT_LANGUAGE. "' THEN 0.5 ";
    $order_by .= "CASE $cases \nEND DESC";

    $location_values = "'" . implode("', '", $locations) . "'";

    // Why not use the DB API? This correlated sub-query needs to have the
    // ORDER BY expression in the ORDER BY statement area and not as an aliased
    // expression like Drupal insists on so that we get one result for the WHERE.
    $where_expression = <<<SQL
$field_data_table.langcode = (
  SELECT CONCAT($extract_language_from_variation_code, '-', vr.delta) AS langcode
  FROM $field_data_table fdt
  LEFT JOIN $variant_reference vr ON SUBSTRING_INDEX(fdt.langcode, '-', -1) = vr.delta AND vr.entity_id = $field_data_table.$id_key
  INNER JOIN $location_field vl ON vl.locations_value IN ($location_values) AND vl.entity_id = vr.variants_target_id
  WHERE fdt.$id_key = $field_data_table.$id_key
  $order_by
  LIMIT 1 OFFSET 0
)
SQL;

    $this->query->addWhereExpression(0, $where_expression);
  }

  protected function invalidLangcode(string $language, string $field_data_table_alias, string $id_key) {
    // If we don't have a variation code (language + location) URL prefix,
    // display an error and show no results.
    $this->messenger()->addWarning($this->t('The Language is set to %lang, but we need a full Language + Location Code in the URL like %example.', [
      '%lang' => $language,
      '%example' => 'en-US',
    ]));
    $this->query->addWhere(0, $field_data_table_alias . '.' . $id_key, 0);
  }

}
