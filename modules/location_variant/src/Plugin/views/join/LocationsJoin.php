<?php

namespace Drupal\location_variant\Plugin\views\join;

use Drupal\views\Plugin\views\join\JoinPluginBase;

/**
 * Provides a "table.column in ('value1', 'value2')" join.
 *
 * @ingroup views_join_handlers
 *
 * @ViewsJoin("location_join")
 */
class LocationsJoin extends JoinPluginBase {

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->right_query = $this->configuration['right_query'];
  }

  public function buildJoin($select_query, $table, $view_query) {
    if (empty($this->configuration['table formula'])) {
      $right_table = "{" . $this->table . "}";
    }
    else {
      $right_table = $this->configuration['table formula'];
    }

    // Add our join condition, using a subquery on the left instead of a field.
    $condition = "$table[alias].$this->field IN ($this->right_query)";
    $arguments = [];

    // Tack on the extra.
    if (isset($this->extra)) {
      // Should we make the left table more generic?
      $left_table = [
        'alias' => 'vr',
      ];
      $this->joinAddExtra($arguments, $condition, $table, $select_query, $left_table);
    }

    $select_query->addJoin($this->type, $right_table, $table['alias'], $condition, $arguments);
  }

}
