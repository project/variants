<?php

namespace Drupal\location_variant\Plugin\Derivative;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic local tasks for localization.
 */
class LocationLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  protected string $basePluginId;
  private ImmutableConfig $config;

  public function __construct($base_plugin_id, ConfigFactoryInterface $config_factory) {
    $this->basePluginId = $base_plugin_id;
    $this->config = $config_factory->get('variants.settings');
  }

  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $enabled_entity_types = $this->config->get('entity_types');
    if ($enabled_entity_types) {
      foreach (array_keys($enabled_entity_types) as $entity_type) {
        // Create tabs for localized entity types.
        $translation_route_name = "entity.$entity_type.content_translation_overview";
        $this->derivatives[$translation_route_name] = [
          'entity_type' => $entity_type,
          'title' => $this->t('Localization'),
          'route_name' => $translation_route_name,
          'base_route' => "entity.$entity_type.canonical",
        ] + $base_plugin_definition;
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
