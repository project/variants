<?php

namespace Drupal\location_variant\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Modifies the 'Add Localization' local action.
 */
class VariantAdd extends LocalActionDefault {

  private CurrentRouteMatch $routeMatch;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.route_provider'),
      $container->get('current_route_match')
    );
  }

  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteProviderInterface $route_provider, CurrentRouteMatch $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->routeProvider = $route_provider;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    // Set the route parameters for the Add Localization local action.
    $parameters = parent::getRouteParameters($route_match);
    $parameters['entity_type'] = $route_match->getParameter('entity_type_id');
    $entity = $route_match->getParameter($parameters['entity_type']);
    $parameters['entity_id'] = $entity->id();
    $parameters['source'] = $entity->langcode->value;

    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    $entity_type_id = $this->routeMatch->getParameter('entity_type_id');
    return "entity.$entity_type_id.location.add";
  }

}
