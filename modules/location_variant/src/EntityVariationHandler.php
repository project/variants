<?php

namespace Drupal\location_variant;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Element;
use Drupal\variants\Entity\Variant;
use Drupal\variants\VariantHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityHandlerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\location_variant\Event\CanEditDefaultEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Entity location handler.
 */
class EntityVariationHandler extends EntityHandlerBase {

  private ?Request $request;
  public VariantHandler $variantHandler;
  public LocationHandler $locationHandler;
  private RouteMatchInterface $routeMatch;
  private EventDispatcherInterface $eventDispatcher;
  public EntityRepositoryInterface $entityRepository;
  public EntityTypeManagerInterface $entityTypeManager;

  public function __construct(VariantHandler $variant_handler, EntityTypeManagerInterface $entity_type_manager, LocationHandler $location_handler, RouteMatchInterface $route_match, RequestStack $request_stack, EventDispatcherInterface $event_dispatcher) {
    $this->variantHandler = $variant_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->locationHandler = $location_handler;
    $this->routeMatch = $route_match;
    $this->request = $request_stack->getCurrentRequest();
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Get Variants.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   *
   * @return \Drupal\variants\Entity\Variant[]
   *   An array of variants for the entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getVariants(EntityInterface $entity): array {
    if ($entity->isNew()) {
      return [];
    }
    return $this->entityTypeManager->getStorage('variant')->loadByProperties([
      'type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
    ]);
  }

  /**
   * Get variant by variation code.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   * @param string $variation_code
   *   The variation code.
   *
   * @return \Drupal\variants\Entity\Variant|null
   *   The variant.
   */
  public function getVariantByVariationCode(EntityInterface $entity, string $variation_code): ?Variant {
    $variant_delta = LocationHandler::parseCode($variation_code)['location'];

    $variants = $entity->get('variants')->referencedEntities();
    return $variants[$variant_delta] ?? NULL;
  }

  /**
   * Get variations.
   *
   * Get all existing and potential variations of this entity. Existing variations are comprised of the localizations and
   * translations of an entity. A potential variation is when a location supports languages, but the translation hasn't
   * been created yet.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to retrieve variations of.
   *
   * @return array
   *   An array of variation info.
   *   - langcode: The langcode as it would appear in the URL e.g. en-US or fr-CA
   *   - variation: Either the localization or the translation (variations) of the entity if it exists. If it does not
   *   exists we pass along the localization so it can be used as a source for a new translation.
   *   - variant: The related variant entity.
   *   - variation code: The variation code. @see generateVariationCode().
   *   - exists: Whether the variation exists or not_exists (a potential translation).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getVariations(EntityInterface $entity) {
    $variations = [];
    foreach ($this->getVariants($entity) as $variant) {
      // Get the localized variation.
      $localization_langcode = $this->generateVariationCode(LocationHandler::DEFAULT_LANGUAGE, $entity, $variant);
      $localized_version = $variant->getVariation($localization_langcode);
      if (empty($localized_version)) {
        // If there is no localized version pass along the default version.
        $localized_version = $variant->getVariation(LocationHandler::DEFAULT_VARIATION_CODE);
      }
      // Build a list of translations or possible translations of this localized variation.
      $supported_langcodes = $variant->getSupportedLangcodes();
      foreach ($supported_langcodes as $langcode) {
        $variation_code = $this->generateVariationCode(LocationHandler::parseCode($langcode)['langcode'], $entity, $variant);
        $localized_translation = $variant->getVariation($variation_code);
        $variation_info = [
          'variation' => $localized_translation ? $localized_translation : $localized_version,
          'variant' => $variant,
          'variation code' => $variation_code,
          'exists' => !empty($localized_translation),
        ];
        $variations[$langcode] = $variation_info;
      }
    }

    return $variations;
  }

  /**
   * Get all variation canonical links.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Link[]
   *   An array of canonical links to the different variation of this entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getVariationCanonicalLinks(EntityInterface $entity) {
    $canonical_links = [];
    $variations = $this->getVariations($entity);
    foreach ($variations as $langcode => $variation_info) {
      if ($variation_info['exists']) {
        $url = $this->getVariationCanonicalUrl($variation_info['variation'], $langcode);
        $canonical_links[$langcode] = Link::fromTextAndUrl($langcode, $url);
      }
    }
    return $canonical_links;
  }

  /**
   * Get all variation edit links.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   An array of variation links. The destination will be either the add or edit page depending on if it exists. A
   *   dynamic "exists" property is added to the link to flag whether it exists already or not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getVariationEditLinks(EntityInterface $entity) {
    $variations = $this->getVariations($entity);
    $edit_links = [];
    foreach ($variations as $langcode => $variation_info) {
      if (\is_null($variation_info['variation'])) {
        continue;
      }
      if ($variation_info['exists']) {
        $url = $this->getVariationEditUrl($variation_info['variation'], $variation_info['variant'], $variation_info['variation code'], $langcode);
      }
      else {
        $url = $this->getVariationAddUrl($variation_info['variation'], $variation_info['variation code'], $langcode);
      }
      $edit_links[$langcode] = Link::fromTextAndUrl($langcode, $url);
      $edit_links[$langcode]->exists = $variation_info['exists'];
    }
    return $edit_links;
  }

  /**
   * Get variation canonical URL.
   *
   * @param \Drupal\Core\Entity\EntityInterface $variation
   *   The entity.
   * @param string $langcode
   *   The langcode that the link should use. e.g. en-US or fr-CA.
   *
   * @return \Drupal\Core\Url
   *   The view variation (translation) URL.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getVariationCanonicalUrl(EntityInterface $variation, string $langcode) {
    $url = $variation->toUrl('canonical');
    $url->setOption('language', \Drupal::languageManager()->getLanguage($langcode));
    $url->setOption('query', \Drupal::destination()->getAsArray());

    return $url;
  }

  /**
   * Get variation edit URL.
   *
   * @param \Drupal\Core\Entity\EntityInterface $variation
   *   The entity.
   * @param \Drupal\variants\Entity\Variant $variant
   *   The related variant.
   * @param string $variation_code
   *   The variation code @see generateVariationCode().
   * @param string $langcode
   *   The langcode that the link should use. e.g. en-US or fr-CA.
   *
   * @return \Drupal\Core\Url
   *   A URL to the appropriate edit page depending on whether the entity variation is a translation or a localization.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getVariationEditUrl(EntityInterface $variation, Variant $variant, string $variation_code, string $langcode) {
    $localization_langcode = $this->generateVariationCode(LocationHandler::DEFAULT_LANGUAGE, $variation, $variant);
    $url = $variation_code === $localization_langcode ? $variation->toUrl('localization.edit') : $variation->toUrl('translation.edit');
    $url->setRouteParameter('language', $variation_code);
    $url->setOption('query', \Drupal::destination()->getAsArray());
    $url->setOption('language', \Drupal::languageManager()->getLanguage($langcode));

    return $url;
  }

  /**
   * Get variation add URL.
   *
   * @param \Drupal\Core\Entity\EntityInterface $variation
   *   The entity.
   * @param string $variation_code
   *   The variation code @see generateVariationCode().
   * @param string $langcode
   *   The langcode that the link should use. e.g. en-US or fr-CA.
   * @param bool $destination
   *   Whether to include the destination query in the URL.
   *
   * @return \Drupal\Core\Url
   *   The URL to the add translation page.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getVariationAddUrl(EntityInterface $variation, string $variation_code, string $langcode, bool $destination = TRUE) {
    $closest_variation = $this->entityRepository()->getTranslationFromContext($variation, $langcode);
    $url = $variation->toUrl('translation.add');
    $url->setRouteParameter('source', $closest_variation->language()->getId());
    $url->setRouteParameter('target', $variation_code);
    $url->setOption('language', \Drupal::languageManager()->getLanguage($langcode));
    if ($destination) {
      $url->setOption('query', \Drupal::destination()->getAsArray());
    }

    return $url;
  }

  /**
   * Get variation delete URL.
   *
   * @param \Drupal\Core\Entity\EntityInterface $variation
   *   The entity.
   * @param string $variation_code
   *   The variation code @see generateVariationCode().
   *
   * @return \Drupal\Core\Url
   *   The URL to the delete variation form.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getVariationDeleteUrl(EntityInterface $variation, string $variation_code) {
    $url = $variation->toUrl('delete-form');
    $url->setRouteParameter('language', $variation_code);
    $url->setOption('query', \Drupal::destination()->getAsArray());

    return $url;
  }

  /**
   * On entity insert.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function insert(EntityInterface $entity) {
    if ($this->variantHandler->hasEnabledVariations($entity)) {
      $variants = $this->getVariants($entity);
      if (empty($variants)) {
        // Create a global variant.
        $locations = [LocationHandler::DEFAULT_LOCATION => $this->locationHandler->getLocation(LocationHandler::DEFAULT_LOCATION)];
        $this->createVariant($entity, $locations);
        $translation = $entity->getTranslation($entity->language()->getId());
        $translation->save();
      }
    }
  }

  /**
   * Create Variant.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create a variation for.
   * @param \Drupal\location_variant\Location[] $locations
   *   An array of locations.
   *
   * @return \Drupal\variants\Entity\Variant
   *   The newly created Variant entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createVariant(EntityInterface $entity, array $locations = [], bool $save = TRUE): Variant {
    $supported_languages = [];
    foreach ($locations as $location) {
      $supported_languages = \array_unique(\array_merge($supported_languages, \array_keys($location->getLanguages())), \SORT_REGULAR);
    }
    $variant = Variant::create([
      'entity_id' => $entity->id(),
      'type' => $entity->getEntityTypeId(),
      'locations' => \array_keys($locations),
      'supported_languages' => $supported_languages,
    ]);
    if ($save) {
      $variant->save();
    }

    $variants = $entity->get('variants')->getValue();
    $variants[] = ['target_id' => $variant->id()];
    $entity->set('variants', $variants);

    return $variant;
  }

  /**
   * Entity create.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function entityCreate(EntityInterface $entity) {
    if ($entity->isNew() && $this->variantHandler->hasEnabledVariations($entity)) {
      // Set the default variation code.
      $entity->set('langcode', LocationHandler::DEFAULT_VARIATION_CODE);
    }
  }

  /**
   * Entity update.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function entityUpdate(EntityInterface $entity) {
    // Prevent an update loop.
    if (isset($entity->flag_variants_updated)) {
      return;
    }
    if ($this->variantHandler->hasEnabledVariations($entity)) {
      // Was a variation (localization or translation) just deleted?
      $deleted_variations = \array_diff_key($entity->original->getTranslationLanguages(), $entity->getTranslationLanguages());
      $need_to_save = FALSE;
      $orphan_variants = [];
      foreach ($deleted_variations as $deleted_variation_code => $language) {
        if (!\str_contains($deleted_variation_code, '-')) {
          continue;
        }
        $langcode_info = LocationHandler::parseCode($deleted_variation_code);
        $deleted_langcode = $langcode_info['langcode'];
        $deleted_variant_delta = $langcode_info['location'];

        // If this entity is the default localization.
        if ($deleted_langcode === LocationHandler::DEFAULT_LANGUAGE) {
          // This variant is now an orphan since the default localization is required.
          $orphan_variants[] = $entity->get('variants')->referencedEntities()[$deleted_variant_delta];

          // Delete the translations of this localization.
          foreach ($entity->getTranslationLanguages() as $code => $l) {
            $langcode_info = LocationHandler::parseCode($deleted_variation_code);
            $lang = $langcode_info['langcode'];
            $delta = $langcode_info['location'];
            if ($deleted_langcode !== $lang && $deleted_variant_delta === $delta) {
              $entity->removeTranslation($code);
              $need_to_save = TRUE;
            }
          }

          // Re-order the variation code (langcode in the DB) since there will
          // be a gap after the variation (localization or translation) has been
          // deleted. e.g. en-1, en-2, en-3. If we remove en-2 and the next assigned
          // variation code is en-4, we would have decreased the number of variations
          // for this entity since the number of variations is limited by configuration.
          $variant_references = $entity->get('variants')->getValue();
          $variant_references_map = [];
          $translation_languages = $entity->getTranslationLanguages();
          \ksort($translation_languages);
          foreach ($translation_languages as $current_code => $current_language) {
            $langcode_info = LocationHandler::parseCode($current_code);
            $current_langcode = $langcode_info['langcode'];
            $current_variant_delta = $langcode_info['location'];
            // If the current variation delta is greater than the delta that was
            // removed.
            if ($current_variant_delta > $deleted_variant_delta) {
              // Move the variation delta down by one so there is no gap by
              // creating a new translation and removing the old one.
              $reordered_delta = $current_variant_delta - 1;
              $reordered_variation_code = "$current_langcode-$reordered_delta";
              $this->variantHandler->prepareVariation($entity, $current_code, $reordered_variation_code);
              $entity->removeTranslation($current_code);

              // Re-order the variant references.
              if (isset($variant_references) && !isset($variant_references_map[$reordered_delta])) {
                $variant_references[$reordered_delta] = $variant_references[$current_variant_delta];
                unset($variant_references[$current_variant_delta]);
                $variant_references_map[$reordered_delta] = $reordered_delta;
              }
              $need_to_save = TRUE;
            }
          }

          $entity->set('variants', $variant_references);
        }
      }
      // Delete the orphan Variants.
      foreach ($orphan_variants as $orphan_variant) {
        if ($orphan_variant) {
          $orphan_variant->delete();
        }
      }
      if ($need_to_save) {
        // Set a flag so that we can prevent an update loop from saving the
        // entity in an update hook.
        $entity->flag_variants_updated = TRUE;
        $entity->save();
        Cache::invalidateTags(["{$entity->getEntityTypeId()}:{$entity->id()}"]);
      }
    }
  }

  /**
   * Form alter for localized entities.
   */
  public function entityFormAlter(array &$form, FormStateInterface $form_state, EntityInterface $entity) {
    if (!$this->variantHandler->hasEnabledVariations($entity)) {
      return;
    }

    $default_langcode = LocationHandler::DEFAULT_LANGUAGE . '-' . LocationHandler::DEFAULT_LOCATION;
    $form_object = $form_state->getFormObject();
    $operation = $form_object->getOperation();
    $current_language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $variations = $this->getVariations($entity);
    $code_info = LocationHandler::parseCode($current_language);

    $event = $this->eventDispatcher->dispatch(new CanEditDefaultEvent($entity, $form_object->getFormId()));
    if (!$event->canEditDefault()) {
      // Users can get to this edit form various ways. Ensure that the user can
      // use this edit form with the provided route and parameters.
      $cant_edit_message = NULL;
      // Are we editing existing content and using a langcode that doesn't have a
      // translation or localization yet?
      if (
        !in_array($operation, ['add', 'default']) &&
        (empty($variations[$current_language]) || ($variations[$current_language]['exists'] === FALSE))
      ) {
        // Show a message if the user is using cores default langcode.
        if ($current_language === LocationHandler::DEFAULT_LANGUAGE) {
          $cant_edit_message = t("You are currently using the langcode prefix <em>en</em>. Please use langcode prefixes like <em>en-US</em> or <em>fr-CA</em> when editing @entity_types.", [
            '@entity_type' => $entity->getEntityType()->getLabel(),
          ]);
        }
        // The user is trying to edit a translation that doesn't exist.
        elseif (!$entity->isNew()) {
          $create_it_url = Url::fromRoute('location_variant.create_variation', [
            'entity_type_id' => $entity->getEntityTypeId(),
            'id' => $entity->id(),
          ])->toString();
          $cant_edit_message = t("I'm sorry. I couldn't find the %current_lang @type for this @entity_type. Would you like to <a href='@edit_url'>edit the default %default_lang</a> or <a href='@create_url'>create the %current_lang @type</a>?", [
            '%current_lang' => $current_language,
            '@type' => $code_info['variation_type'],
            '@entity_type' => $entity->getEntityType()->getLabel(),
            '@edit_url' => $this->getVariationEditUrl($variations[$default_langcode]['variation'], $variations[$default_langcode]['variant'], $variations[$default_langcode]['variation code'], $default_langcode)->toString(),
            '%default_lang' => $default_langcode,
            '@create_url' => $create_it_url,
          ]);
        }
      }
      // The user is trying to add a translation to a new piece of content which
      // doesn't have a default yet. A default version is required before adding
      // translations.
      elseif ($entity->isNew() && ($current_language !== $default_langcode)) {
        $cant_edit_message = t("There needs to be a default @default_lang version before adding a @current_lang version. Would you like to <a href='@url'>create it</a>?", [
          '@default_lang' => $default_langcode,
          '@current_lang' => $current_language,
          '@url' => Url::fromRouteMatch($this->routeMatch)
            ->setOption('language', \Drupal::languageManager()->getLanguage($default_langcode))
            ->toString(),
        ]);
      }
      if ($cant_edit_message) {
        \Drupal::messenger()->addWarning($cant_edit_message);
        $form['#access'] = FALSE;
        $form['#markup'] = $cant_edit_message;
        $form['#cache'] = [
          'tags' => [],
          'contexts' => [],
          'max-age' => 0,
        ];
        return;
      }
    }

    if (!$entity->isDefaultTranslation()) {
      // Hide translatable fields as they can only be edited on the default translation.
      foreach(Element::children($form) as $field) {
        if ($entity->hasField($field)) {
          if (!$entity->get($field)->getFieldDefinition()->isTranslatable()) {
            $form[$field]['#access'] = FALSE;
          }
        }
      }
    }

    if ($operation == 'edit') {
      $form_langcode = $form_state->getFormObject()->getFormLangcode($form_state);
      $entity_langcode = $entity->getUntranslated()->language()->getId();
      $new_translation = $entity->isNewTranslation();
      $translations = $entity->getTranslationLanguages();
      $is_translation = $new_translation || ($entity->language()->getId() != $entity_langcode);
      if ($new_translation) {
        // Make sure a new translation does not appear as existing yet.
        unset($translations[$form_langcode]);
      }
      $has_translations = \count($translations) > 1;
      // Adjust page title to specify that we are adding/editing a Localization.
      $languages = \Drupal::languageManager()->getLanguages();
      if (isset($languages[$form_langcode]) && ($has_translations || $new_translation)) {
        $title = $entity->label();
        // When editing the original values display just the entity label.
        if ($is_translation) {
          $language = $languages[$form_langcode];
          // Get locations from variant.
          $variant = $this->getVariantByVariationCode($entity, $form_langcode);
          if ($variant) {
            $locations = $variant->getLocations();
          }
          // Get locations from URL.
          elseif ($location_ids = $this->request->query->get('locations')) {
            foreach ($location_ids as $location_id) {
              $location = $this->locationHandler->getLocation($location_id);
              $locations[$location->getId()] = $location;
            }
          }
          else {
            /** @var LocationNegotiator $negotiator */
            $negotiator = \Drupal::service('location_variant.negotiator');
            $current_language = $negotiator->getCurrentLanguage($this->request);
            $location_id = LocationHandler::parseCode($current_language->getId())['location'];
            $location = $this->locationHandler->getLocation($location_id);
            $locations[$location->getId()] = $location;
          }
          // Account for multiple locations in the title.
          $location_label = '';
          if (isset($locations) && \count($locations) > 1) {
            $location_labels = [];
            foreach ($locations as $location) {
              $location_labels[] = $location->getLabel();
            }
            $last = array_pop($location_labels);
            $first = implode(', ', $location_labels);
            $both = array_filter([$first, $last]);
            $location_label = implode(' and ', $both);
          }
          else {
            $location = \reset($locations);
            $location_label = $location->getLabel();
          }
          $localization_or_translation = in_array($code_info['variation_type'], ['default', 'localization']) ? 'localization' : 'translation';
          $t_args = [
            '@language' => $language->getName(),
            '@location' => $location_label,
            '@type' => $localization_or_translation,
            '%title' => $title,
            '@op' => $new_translation ? 'Create' : 'Edit',
          ];
          if ($localization_or_translation === 'localization') {
            $title = $this->t("@op @location's @type of %title", $t_args);
          }
          elseif ($localization_or_translation === 'translation') {
            $title = $this->t('@op @language @type of %title for @location', $t_args);
          }
          $form['actions']['submit']['#value'] = $this->t('Save @type', ['@type' => $localization_or_translation]);
        }
        $form['#title'] = $title;
      }
    }
    foreach (Element::children($form['actions']) as $action) {
      if (isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] == 'submit') {
        // Handle entity form submission before the entity has been saved.
        \array_unshift($form['actions'][$action]['#submit'], [$this, 'entityFormSubmit']);

        $form['actions'][$action]['#submit'][] = [$this, 'redirect'];
      }
    }
  }

  /**
   * Submit handler.
   */
  public function entityFormSubmit($form, FormStateInterface $form_state) {
    if (!isset($this->routeMatch)) {
      // Route Match doesn't seem to be set when changing a node's image field.
      $this->routeMatch = \Drupal::routeMatch();
    }
    if (!isset($this->request)) {
      // Request doesn't seem to be set when changing a node's image field.
      $this->request = \Drupal::request();
    }
    // If this is an add variation route.
    $route_pieces = \explode('.', $this->routeMatch->getRouteName());
    if (isset($route_pieces[2]) && ($localization_or_translation = $route_pieces[2]) && \in_array($localization_or_translation, ['localization', 'translation']) && $route_pieces[3] === 'add') {
      $location_ids = $this->request->query->get('locations');
      if ($location_ids) {
        $locations = [];
        foreach ($location_ids as $location_id) {
          $locations[$location_id] = $this->locationHandler->getLocation($location_id);
        }
        $entity = $form_state->getFormObject()->getEntity();
        $this->createVariant($entity, $locations);
      }
    }
  }

  /**
   * Submit handler.
   */
  public function redirect($form, FormStateInterface $form_state) {
    // Update the language in the redirect from the variation code to langcode.
    $entity = $form_state->getFormObject()->getEntity();
    $url = $entity->toUrl();
    $variation_code = $entity->language()->getId();
    if (\str_contains($variation_code, '-')) {
      $variant = $this->getVariantByVariationCode($entity, $variation_code);
      $langcode = LocationHandler::parseCode($variation_code)['langcode'];
      $locations = $variant->getLocations();
      $langcode = $this->locationHandler->generateLangcode(\array_key_first($locations), $langcode);
      $url->setOption('language', \Drupal::languageManager()->getLanguage($langcode));
      $form_state->setRedirectUrl($url);
    }
  }

  /**
   * Generate variation code.
   *
   * The translation system is used for both localizations and translations. It relates an entity to it's variant. The
   * variation codes are stored in the langcode column in the field_data table for an entity in the database. It is in
   * the format of LANDCODE-DELTA e.g. en-0 or fr-3 where delta is the delta in the entities Variants entity reference
   * field.
   *
   * @param string $langcode
   *   The language code.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\variants\Entity\Variant $variant
   *   The variant entity.
   *
   * @return string
   *   The variation code.
   */
  public function generateVariationCode(string $langcode, EntityInterface $entity, Variant $variant = NULL): string {
    $variants = $entity->get('variants')->getValue();
    // Find this variants delta.
    if ($variant) {
      foreach ($variants as $delta => $value) {
        if ($variant->id() === $value['target_id']) {
          break;
        }
      }
    }
    // Find the next available delta.
    else {
      $delta = \count($variants);
    }
    if (!isset($delta)) {
      // Let's set a default delta in the case we can't find one such as listing
      // variations in another workspace.
      $delta = 0;
    }
    return "$langcode-$delta";
  }

  /**
   * Entity Repository.
   *
   * For some reason injecting the entity repository makes sites no longer able
   * to update.
   *
   * @return \Drupal\Core\Entity\EntityRepositoryInterface
   *   The entity repository service.
   */
  protected function entityRepository(): EntityRepositoryInterface {
    if (!isset($this->entityRepository)) {
      $this->entityRepository = \Drupal::service('entity.repository');
    }
    return $this->entityRepository;
  }

}
