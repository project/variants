<?php

namespace Drupal\location_variant\EventSubscriber;

use Drupal\Core\Language\Language;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\variants\Event\LanguagesEvent;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Languages event subscriber.
 */
class VariationLanguageCodes implements EventSubscriberInterface {

  private Connection $connection;
  private ImmutableConfig $settings;
  private LocationHandler $locationHandler;

  public function __construct(LocationHandler $location_handler, Connection $connection, ConfigFactoryInterface $config) {
    $this->locationHandler = $location_handler;
    $this->connection = $connection;
    $this->settings = $config->get('variants.settings');
  }

  /**
   * On get languages.
   *
   * @param \Drupal\variants\Event\LanguagesEvent $event
   *   The languages event.
   */
  public function onGetLanguages(LanguagesEvent $event) {
    if (!$this->connection->schema()->tableExists('variant_revision__locations')) {
      return;
    }
    $locations = $this->locationHandler->getLocations();
    $languages = $event->getLanguages();
    // Set the default location.
    if (isset($locations[LocationHandler::DEFAULT_LOCATION])) {
      $languages[LocationHandler::DEFAULT_LANGUAGE]->locations[LocationHandler::DEFAULT_LOCATION] = $locations[LocationHandler::DEFAULT_LOCATION];
    }

    // Add a language for every permutation of location and it's supported
    // languages. These langcodes will be used as URL prefixes.
    foreach ($locations as $location_id => $location) {
      foreach ($location->getLanguages() as $langcode => $values) {
        $new_langcode = $this->locationHandler->generateLangcode($location_id, $langcode);
        $variation_language = $this->newLanguage($new_langcode, $values['label'], $values['direction'] ?? 'ltr');
        $event->addLanguage($variation_language);
      }
    }

    // Add a langcode + delta language for each supported language. This is the
    // langcode that is stored in the database. Entities with variation support
    // enabled get a variant reference field added to it. This language + delta
    // code is used to point to the correct variant reference field delta.
    $languages_and_deltas = [];
    $variant_limit = $this->settings->get('variant_limit') ?? 10;
    foreach ($locations as $location) {
      foreach ($location->getLanguages() as $langcode => $values) {
        if (!isset($languages_and_deltas[$langcode])) {
          $languages_and_deltas[$langcode] = 0;
          for ($i = 0; $i < $variant_limit; $i++) {
            $delta_language = $this->newLanguage("$langcode-$i", $values['label'], $values['direction'] ?? 'ltr');
            $event->addLanguage($delta_language);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LanguagesEvent::class => ['onGetLanguages', 100],
    ];
  }

  /**
   * New language.
   *
   * @param string $id
   *   The language ID.
   * @param string $name
   *   The language name.
   * @param string $direction
   *   The langauge dircetion.
   *
   * @return \Drupal\Core\Language\Language
   *   A new language.
   */
  private function newLanguage(string $id, string $name, string $direction = 'ltr') {
    return new Language([
      'id' => $id,
      'name' => $name,
      'direction' => $direction,
    ]);
  }

}
