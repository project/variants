<?php

namespace Drupal\location_variant;

use Drupal\path_alias\AliasRepository;

/**
 * Implements variant aware path alias lookup queries.
 */
class LocationVariantAliasRepository extends AliasRepository {

  use LocationVariantAliasRepositoryTrait;

}
