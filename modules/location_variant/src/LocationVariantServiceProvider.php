<?php

namespace Drupal\location_variant;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Defines a service provider for the Location Variant module.
 */
class LocationVariantServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Replace the class of the 'path_alias.repository' service.
    if ($container->hasDefinition('path_alias.repository')) {
      $definition = $container->getDefinition('path_alias.repository');

      if ($definition->isDeprecated()) {
        return;
      }

      $modules = $container->getParameter('container.modules');
      if (\array_key_exists('workspaces', $modules)) {
        $definition
          ->setClass(LocationVariantWorkspacesAliasRepository::class)
          ->addMethodCall('setWorkspacesManager', [new Reference('workspaces.manager')]);
      }
      else {
        $definition->setClass(LocationVariantAliasRepository::class);
      }
    }
  }

}
