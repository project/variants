<?php

namespace Drupal\location_variant\Event;

use Drupal\location_variant\Location;
use Drupal\Component\EventDispatcher\Event;

class LocationsEvent extends Event {

  protected array $locations = [];

  public function addLocation(string $label, string $id, array $languages, ?string $parent, ?string $alias = NULL) {
    $this->locations[$id] = new Location($label, $id, $languages, $this->locations[$parent] ?? NULL, $alias ?? NULL);
  }

  public function getLocation(string $id): ?Location {
    return $this->locations[$id] ?? NULL;
  }

  public function setLocations(Location ...$locations): void {
    $this->locations = $locations;
  }

  public function getLocations(): array {
    return $this->locations;
  }

}
