<?php

namespace Drupal\location_variant\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\EventDispatcher\Event;

class CanEditDefaultEvent extends Event {

  protected bool $canEdit = FALSE;

  private EntityInterface $entity;
  private string $form_id;

  public function __construct(EntityInterface $entity, string $form_id) {
    $this->entity = $entity;
    $this->form_id = $form_id;
  }

  /**
   * @return bool
   */
  public function canEditDefault(): bool {
    return $this->canEdit;
  }

  /**
   * @param bool $canEdit
   */
  public function canEdit(): void {
    $this->canEdit = TRUE;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function setEntity(EntityInterface $entity): void {
    $this->entity = $entity;
  }

  /**
   * @return string
   */
  public function getFormId(): string {
    return $this->form_id;
  }

  /**
   * @param string $form_id
   */
  public function setFormId(string $form_id): void {
    $this->form_id = $form_id;
  }

}
