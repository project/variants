<?php

namespace Drupal\location_variant\Form;

use Drupal\Core\Config\Config;
use Drupal\field\FieldConfigInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

class VariantSettingsFormAlter {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  protected Config $config;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;
  protected EntityFieldManagerInterface $entityFieldManager;


  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->config = $config_factory->getEditable('variants.settings');
    $this->entityFieldManager = $entity_field_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param $form
   *   The variant settings form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function alter(&$form, FormStateInterface $form_state) {
    $config = $this->config->getRawData();
    $entity_types = $this->entityTypeManager->getDefinitions();
    $labels = [];
    $bundles = $this->entityTypeBundleInfo->getAllBundleInfo();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$entity_type instanceof ContentEntityTypeInterface || !$entity_type->hasKey('langcode') || !isset($bundles[$entity_type_id])) {
        continue;
      }
      $labels[$entity_type_id] = $entity_type->getLabel() ?: $entity_type_id;
      $form[$entity_type_id] = [
        '#tree' => TRUE,
        '#type' => 'details',
        '#title' => $entity_type->getLabel(),
        '#open' => FALSE,
        '#weight' => 5,
        '#description' => $this->t('Mark Bundles and fields as translatable.'),
        '#states' => [
          'visible' => [
            ':input[name="entity_types['. $entity_type_id . ']"]' => [
              'checked' => TRUE,
            ],
          ],
        ],
      ];
      foreach ($bundles[$entity_type_id] as $bundle => $bundle_info) {
        $form[$entity_type_id][$bundle]['enabled'] = [
          '#tree' => TRUE,
          '#title' => $bundle_info['label'],
          '#type' => 'checkbox',
          '#default_value' => !empty($config['entity_types'][$entity_type_id]) && in_array($bundle, $config['entity_types'][$entity_type_id]),
        ];
        $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
        $field_labels = [];
        $field_translatable = [];
        foreach ($fields as $field_name => $field_info) {
          $field_labels[$field_name] = $field_info->getLabel() ?: $field_name;
          if ($field_info->isTranslatable()) {
            $field_translatable[$field_name] = $field_name;
          }
        }
        $form[$entity_type_id][$bundle]['fields'] = [
          '#type' => 'container',
          '#states' => [
            'visible' => [
              sprintf(':input[name="%s[%s][enabled]"]', $entity_type_id, $bundle) => ['checked' => TRUE],
            ],
          ],
          'field_translations' => [
            '#type' => 'checkboxes',
            '#options' => $field_labels,
            '#default_value' => $field_translatable ?? [],
          ],
        ];
      }
      $form['entity_types'] = [
        '#title' => $this->t('Localization and Translation'),
        '#type' => 'checkboxes',
        '#options' => $labels,
        '#default_value' => !empty($config['entity_types']) ? array_keys($config['entity_types']) : [],
        '#weight' => -1,
        '#description' => $this->t('Enable localization and translations for content entities.')
      ];
    }

    $form['actions']['submit']['#submit'][] = [$this, 'submitForm'];
    $form['#attached']['library'][] = 'location_variant/settings';
  }

  /**
   * Variant settings form submission handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $previous_entity_types = $this->config->get('entity_types') ?? [];
    $submitted_entity_types = \array_filter($form_state->getValue('entity_types')) ?? [];

    // Get bundle and field values for each entity type.
    $entity_types = [];
    foreach ($submitted_entity_types as $entity_type){
      foreach ($form_state->getValue($entity_type) as $bundle => $values){
        if ($values['enabled']) {
          // Track what bundles are translatable. @see bundleInfoAlter();
          $entity_types[$entity_type][] = $bundle;
        }
        // Update whether a field has translations enabled.
        $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
        foreach ($values['fields']['field_translations'] as $field_name => $translatable) {
          $current_field = $fields[$field_name];
          if ($current_field instanceof FieldConfigInterface) {
            $current_field->setTranslatable($translatable !== 0);
            $current_field->save();
          }
        }
      }
    }

    $this->config
      ->set('entity_types', $entity_types)
      ->set('variant_limit', $form_state->getValue('variant_limit'))
      ->save();

    // Entity type was enabled.
    $enabled_entity_types = \array_diff(array_keys($submitted_entity_types), array_keys($previous_entity_types));
    foreach ($enabled_entity_types as $enabled_entity_type) {
      $entities = $this->entityTypeManager->getStorage($enabled_entity_type)->loadMultiple();
      $operations = [];
      foreach ($entities as $entity) {
        $operations[] = [
          [static::class, 'processEnableBatch'],
          [
            [
              'entity_type' => $entity->getEntityTypeId(),
              'entity_id' => $entity->id(),
            ],
          ],
        ];
      }

      if ($operations) {
        $batch = [
          'operations' => $operations,
          'title' => t('Configuring %entity_type for variations', [
            '%entity_type' => $enabled_entity_type,
          ]),

        ];
        batch_set($batch);
      }
    }

    // Entity type was disabled.
    $disabled_entity_types = \array_diff(array_keys($previous_entity_types), array_keys($submitted_entity_types));
    foreach ($disabled_entity_types as $disabled_entity_type) {
      $entities = $this->entityTypeManager->getStorage($disabled_entity_type)->loadMultiple();
      $operations = [];
      foreach ($entities as $entity) {
        $operations[] = [
          [static::class, 'processDisableBatch'],
          [
            [
              'entity_type' => $entity->getEntityTypeId(),
              'entity_id' => $entity->id(),
            ],
          ],
        ];
      }

      if ($operations) {
        $batch = [
          'operations' => $operations,
          'title' => t('Removing %entity_type configuration for variations', [
            '%entity_type' => $disabled_entity_type,
          ]),

        ];
        batch_set($batch);
      }
    }
  }

  /**
   * Enable variations batch callback.
   *
   * Creates a global variant for entities with existing content.
   */
  public static function processEnableBatch($data, &$context) {
    $entity = \Drupal::entityTypeManager()->getStorage($data['entity_type'])->load($data['entity_id']);
    $entity->set('langcode', LocationHandler::DEFAULT_VARIATION_CODE);
    \Drupal::service('location_variant.entity_handler')->insert($entity);
  }

  /**
   * Disable variations batch callback.
   */
  public static function processDisableBatch($data, &$context) {
    $entity = \Drupal::entityTypeManager()->getStorage($data['entity_type'])->load($data['entity_id']);
    // Remove variations of this entity.
    foreach ($entity->getTranslationLanguages() as $variation_code => $l) {
      if (!$entity->getTranslation($variation_code)->isDefaultTranslation()) {
        $entity->removeTranslation($variation_code);
      }
    }
    // Remove orphaned variants.
    $variants = \Drupal::service('location_variant.entity_handler')->getVariants($entity);
    foreach ($variants as $variant) {
      $variant->delete();
    }
    // Change the variation code back to the default langcode.
    $entity->set('langcode', \Drupal::languageManager()->getDefaultLanguage()->getId());
    $entity->save();
  }

}
