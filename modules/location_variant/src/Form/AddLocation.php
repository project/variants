<?php

namespace Drupal\location_variant\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\location_variant\EntityVariationHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects locations before creating a Localization.
 */
class AddLocation extends FormBase {

  protected EntityVariationHandler $entityVariationHandler;
  private LocationHandler $locationHandler;
  private PrivateTempStore $tempStore;
  protected EntityTypeManagerInterface $entityTypeManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('location_variant.entity_handler'),
      $container->get('location_variant.location_handler'),
      $container->get('tempstore.private'),
      $container->get('entity_type.manager')
    );
  }

  public function __construct(EntityVariationHandler $entity_location_handler, LocationHandler $location_handler, PrivateTempStoreFactory $temp_store, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityVariationHandler = $entity_location_handler;
    $this->locationHandler = $location_handler;
    $this->tempStore = $temp_store->get('location_variant');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'location_add_location';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('workspace_safe', TRUE);
    // Check that we haven't reached the variant limit.
    $route_match = $this->getRouteMatch();
    $entity_type_id = $route_match->getParameter('entity_type_id');
    $entity_id = $route_match->getRawParameter($entity_type_id);
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    $variants = $this->entityVariationHandler->getVariants($entity);
    $max_variants = $this->config('variants.settings')->get('variant_limit');
    if (count($variants) >= $max_variants) {
      $this->messenger()->addWarning($this->t('This entity already has %max localizations which is the <a href="@settings_url">current maximum</a>.', [
        '%max' => $max_variants,
        '@settings_url' => Url::fromRoute('variants_settings')->toString(),
      ]));
      return new RedirectResponse($entity->toUrl('drupal:content-translation-overview')->toString());
    }

    $location_options = $this->locationHandler->getLocationOptions();
    $location_variants = $this->entityTypeManager->getStorage('variant')->loadByProperties(['entity_id' => $entity_id]);
    if ($location_variants) {
      foreach ($location_variants as $location) {
        $location_code = $location->get('locations')->getValue()[0]['value'] ?? '';
        if ($location_code && isset($location_options[$location_code])) {
          // Mark any already used locations as an option group since disabling
          // single select options is STILL not supported.
          $i = 0;
          foreach ($location_options as $location_id => $label) {
            if ($location_code === $location_id) {
              $location_options = array_slice($location_options, 0, $i, true) +
                [ltrim($label) => []] +
                array_slice($location_options, $i, NULL, true);
              unset($location_options[$location_id]);
              break;
            }
            $i++;
          }
        }
      }
    }

    $form['locations'] = [
      '#type' => 'select',
      '#title' => $this->t('Location'),
      '#options' => $location_options,
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Localization'),
    ];

    $form['#attached']['library'][] = 'location_variant/add';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $route_match = $this->getRouteMatch();
    $entity_type_id = $route_match->getParameter('entity_type_id');
    $entity_id = $route_match->getRawParameter($entity_type_id);
    $locations = $form_state->getValue('locations');
    \sort($locations);
    $source = $route_match->getRawParameter('source');
    $variation_code = $this->entityVariationHandler->generateVariationCode(LocationHandler::DEFAULT_LANGUAGE, $route_match->getParameter($entity_type_id));

    $form_state->setRedirect("entity.$entity_type_id.localization.add", [
      $entity_type_id => $entity_id,
      'source' => $source,
      'target' => $variation_code,
    ], [
      'query' => [
        'locations' => $locations,
      ],
    ]);
  }

}
