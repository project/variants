<?php

namespace Drupal\location_variant\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Delete variation form.
 *
 * @internal
 */
class VariationDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'variation_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    if (!$entity->isDefaultTranslation()) {
      return $this->t('Are you sure you want to delete the @language @variation_type of the @entity-type %label?', [
        '@language' => $entity->language()->getName(),
        '@entity-type' => $this->getEntity()->getEntityType()->getSingularLabel(),
        '%label' => $this->getEntity()->label(),
        '@variation_type' => LocationHandler::parseCode($entity->language()->getId())['langcode'] === LocationHandler::DEFAULT_LANGUAGE ? $this->t('localization') : $this->t('translation'),
      ]);
    }

    return $this->traitGetQuestion();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    $variation_code_to_delete = $entity->language()->getId();

    $variations = [];
    if ($entity->isDefaultTranslation()) {
      if (count($entity->getTranslationLanguages()) > 1) {
        foreach ($entity->getTranslationLanguages() as $language) {
          $variation = $entity->getTranslation($language->getId());
          $variations[] = $variation->label();
        }
      }
    }
    else {
      if (\str_contains($variation_code_to_delete, '-')) {
        [$langcode_to_delete, $delta_to_delete] = \explode('-', $variation_code_to_delete);
        // If this entity is the default localization.
        if ($langcode_to_delete === LocationHandler::DEFAULT_LANGUAGE) {
          // Delete the translations of this localization.
          foreach ($entity->getTranslationLanguages() as $variation_code => $langcode) {
            $delta = LocationHandler::parseCode($variation_code)['location'];
            if ($delta === $delta_to_delete && $variation_code !== $variation_code_to_delete) {
              $variation = $entity->getTranslation($variation_code);
              $variations[] = $variation->label();
            }
          }
        }
      }
    }

    if ($variations) {
      $form['deleted_translations'] = [
        '#theme' => 'item_list',
        '#title' => $this->t('The following @entity-type variations will be deleted:', [
          '@entity-type' => $entity->getEntityType()->getSingularLabel(),
        ]),
        '#items' => $variations,
      ];
      $form['actions']['submit']['#value'] = $this->t('Delete variations');
    }
    else {
      $form['actions']['submit']['#value'] = $this->t('Delete @language @variation_type', [
        '@language' => $entity->language()->getName(),
        '@variation_type' => LocationHandler::parseCode($entity->language()->getId())['langcode'] === LocationHandler::DEFAULT_LANGUAGE ? $this->t('localization') : $this->t('translation'),
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function logDeletionMessage() {
    /** @var \Drupal\node\NodeInterface $entity */
    $entity = $this->getEntity();
    $this->logger('content')->notice('@type: deleted %title.', ['@type' => $entity->getEntityType()->getLabel(), '%title' => $entity->label()]);
  }

}
