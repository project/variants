<?php

namespace Drupal\location_variant;

/**
 * A location object.
 */
class Location {

  protected string $label;
  protected string $id;
  protected array $languages;
  protected ?Location $parent;
  protected ?string $alias;
  protected array $children = [];

  public function __construct(string $label, string $id, array $languages, ?Location $parent, ?string $alias) {
    $this->label = $label;
    $this->id = $id;
    $this->languages = $languages;
    $this->parent = $parent;
    $this->alias = $alias;
  }

  public function getLabel(): string {
    return $this->label;
  }

  public function getId(): string {
    return $this->id;
  }

  public function getAlias(): ?string {
    return $this->alias;
  }

  public function getLanguages(): array {
    return $this->languages;
  }

  public function getParent(): ?Location {
    return $this->parent;
  }

  public function addChild(Location $child) {
    $this->children[$child->getId()] = $child;
  }

  public function getChildren(): array {
    return $this->children;
  }

}
