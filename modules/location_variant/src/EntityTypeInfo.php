<?php

namespace Drupal\location_variant;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

class EntityTypeInfo {

  private ImmutableConfig $settings;
  private EntityVariationHandler $entityVariationHandler;

  public function __construct(ConfigFactoryInterface $factory, EntityVariationHandler $entity_variation_handler) {
    $this->settings = $factory->get('variants.settings');
    $this->entityVariationHandler = $entity_variation_handler;
  }

  /**
   * Entity base field info.
   *
   * Adds localization fields.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by field name.
   *
   * @see hook_entity_base_field_info()
   */
  public function entityBaseFieldInfo(EntityTypeInterface $entity_type) {
    if ($entity_type->id() === 'variant') {

      $fields['locations'] = BaseFieldDefinition::create('string')
        ->setLabel(\t('Locations'))
        ->setDescription(\t('The Locations this entity applies to.'))
        ->setSettings([
          'max_length' => 255,
          'text_processing' => 0,
        ])
        ->setRequired(TRUE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

      $fields['supported_languages'] = BaseFieldDefinition::create('string')
        ->setLabel(\t('Languages'))
        ->setDescription(\t('Languages this entity applied to.'))
        ->setSettings([
          'max_length' => 50,
          'text_processing' => 0,
        ])
        ->setRequired(TRUE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

      return $fields;
    }
  }

  /**
   * Bundle info alter.
   *
   * @see hook_entity_bundle_info_alter()
   */
  public function bundleInfoAlter(&$bundles) {
    $enabled_entity_types = $this->settings->get('entity_types');
    if ($enabled_entity_types) {
      foreach ($enabled_entity_types as $entity_type_id => $bundles_info) {
        foreach ($bundles_info as $bundle) {
          // Mark bundles as translatable for ContentEntityBase->isTranslatable().
          $bundles[$entity_type_id][$bundle]['translatable'] = TRUE;
          // We need to hide untranslatable field widgets, otherwise changes in pending
          // revisions might be overridden by changes in later default revisions.
          $bundles[$entity_type_id][$bundle]['untranslatable_fields.default_translation_affected'] = TRUE;
        }
      }
    }
  }

  /**
   * Entity Type Alter.
   *
   * Add link templates to supported entities.
   */
  public function entityTypeAlter(array &$entity_types) {
    $enabled_entity_types = $this->settings->get('entity_types');
    if ($enabled_entity_types) {
      foreach (array_keys($enabled_entity_types) as $entity_type_id) {
        $entity_type = $entity_types[$entity_type_id];
        if ($entity_type->isTranslatable()) {
          $entity_type->setFormClass('delete', '\Drupal\location_variant\Form\VariationDeleteForm');
          if ($entity_type->hasLinkTemplate('canonical')) {
            if (!$entity_type->hasLinkTemplate('drupal:content-translation-overview')) {
              $canonical = $entity_type->getLinkTemplate('canonical');
              $localizations_path = $canonical . '/localizations';
              $entity_type->setLinkTemplate('drupal:content-translation-overview', $localizations_path);
              $entity_type->setLinkTemplate('location.add', $canonical . '/location/add/{source}');
              $entity_type->setLinkTemplate('localization.add', $localizations_path . '/add/{source}/{target}');
              $entity_type->setLinkTemplate('localization.edit', $localizations_path . '/edit/{language}');
              $translations_path = $entity_type->getLinkTemplate('canonical') . '/translations';
              $entity_type->setLinkTemplate('translation.add', $translations_path . '/add/{source}/{target}');
              $entity_type->setLinkTemplate('translation.edit', $translations_path . '/edit/{language}');
            }
          }
        }
      }
    }
  }

  /**
   * Menu local actions alter.
   */
  public function menuLocalActionsAlter(&$local_actions) {
    if (isset($local_actions['location_reference.add'])) {
      $enabled_entity_types = $this->settings->get('entity_types');
      if ($enabled_entity_types) {
        foreach (array_keys($enabled_entity_types) as $type) {
          // Show the Localization tab.
          $local_actions['location_reference.add']['appears_on'][] = "entity.$type.content_translation_overview";
        }
      }
    }
  }

  /**
   * Form alters the entity form.
   */
  public function formAlter(&$form, FormStateInterface $form_state, $form_id) {
    $form_object = $form_state->getFormObject();
    if (!($form_object instanceof ContentEntityFormInterface)) {
      return;
    }
    $entity = $form_object->getEntity();
    $op = $form_object->getOperation();
    if ($entity instanceof ContentEntityInterface && $this->entityVariationHandler->variantHandler->hasEnabledVariations($entity) && in_array($op, ['edit', 'add', 'default'], TRUE)) {
      $this->entityVariationHandler->entityFormAlter($form, $form_state, $entity);
    }
  }

}
