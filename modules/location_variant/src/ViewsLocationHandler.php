<?php

namespace Drupal\location_variant;

use Drupal\views\ViewExecutable;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;

class ViewsLocationHandler {

  private ImmutableConfig $settings;
  private EntityTypeManagerInterface $entityTypeManager;


  public function __construct(ConfigFactoryInterface $factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->settings = $factory->get('variants.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Implements hook_views_data_alter().
   */
  public function viewsDataAlter(&$data) {
    // Add a variant views reference field for each enabled entity type.
    $enabled_entity_types = $this->settings->get('entity_types');
    if (!empty($enabled_entity_types)) {
      foreach (array_keys($enabled_entity_types) as $entity_type_id) {
        $entity_type = $this->entityTypeManager->getStorage($entity_type_id)->getEntityType();
        $base_table = $entity_type->getBaseTable();
        $data["{$base_table}__variants"]['variant'] = [
          'title' => t('Variations'),
          'help' => t('Show the most specific variation to the user based on URL prefix.'),
          'relationship' => [
            'id' => 'variant',
            'label' => t('Variations'),
            'base' => "{$base_table}__variants",
            'base_revision' => "{$base_table}_revision__variants",
            'field_data' => $entity_type->getDataTable(),
            'field_data_revision' => $entity_type->getRevisionDataTable(),
            'id_key' => $entity_type->getKey('id'),
          ],
        ];
      }
    }
  }

}
