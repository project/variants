<?php

namespace Drupal\location_variant;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Provides method for variant aware path alias lookup queries.
 */
trait LocationVariantAliasRepositoryTrait {

  /**
   * Adds path alias language fallback conditions to a select query object.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   A Select query object.
   * @param string $langcode
   *   Language code to search the path with. If there's no path defined for
   *   that language it will search paths without language.
   */
  protected function addLanguageFallback(SelectInterface $query, $langcode) {
    $langcode_list = [
      LocationHandler::DEFAULT_VARIATION_CODE,
      $langcode,
      LanguageInterface::LANGCODE_NOT_SPECIFIED
    ];

    // Always get the language-specific alias before the language-neutral one.
    // For example 'de' is less than 'und' so the order needs to be ASC, while
    // 'xx-lolspeak' is more than 'und' so the order needs to be DESC.
    if ($langcode === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      array_pop($langcode_list);
    }
    elseif ($langcode > LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      $query->orderBy('base_table.langcode', 'DESC');
    }
    else {
      $query->orderBy('base_table.langcode', 'ASC');
    }
    $query->condition('base_table.langcode', $langcode_list, 'IN');
  }

}
