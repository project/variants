<?php

namespace Drupal\location_variant;

use Drupal\variants\Entity\Variant;
use Drupal\Core\Entity\EntityInterface;
use Drupal\variants\NegotiatorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Language\LanguageInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Location negotiator.
 */
class LocationNegotiator implements NegotiatorInterface {

  private EntityVariationHandler $entityVariationHandler;

  public function __construct(EntityVariationHandler $entity_location_handler) {
    $this->entityVariationHandler = $entity_location_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request = NULL):string {
    $parts = \explode('/', \trim($path, '/'));
    $prefix = \array_shift($parts);
    // Search prefix within added languages.
    $languages = \Drupal::languageManager()->getLanguages();
    if (isset($languages[$prefix])) {
      // Rebuild $path with the language removed.
      $path = '/' . \implode('/', $parts);
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL): string {
    $languages = \Drupal::languageManager()->getLanguages();
    // If there is no language set, or if there is a variation code set, or it
    // is set to und.
    if (
      !isset($options['language'])
      || (isset($options['language']) && LocationHandler::isVariationCode($options['language']->getId()))
      || (isset($options['language']) && $options['language']->getId() === LanguageInterface::LANGCODE_NOT_SPECIFIED)
    ) {
      // Use the current language from the URL.
      $options['language'] = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_URL);
    }
    // We allow only added languages here.
    elseif (!\is_object($options['language']) || !isset($languages[$options['language']->getId()])) {
      return $path;
    }

    $options['prefix'] = $options['language']->getId() . '/';
    if ($bubbleable_metadata) {
      $bubbleable_metadata->addCacheContexts(['languages:' . LanguageInterface::TYPE_URL]);
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentLanguage(Request $request = NULL) {
    $langcode = NULL;

    if ($request) {
      $languages = \Drupal::languageManager()->getLanguages();
      $request_path = \urldecode(\trim($request->getPathInfo(), '/'));
      $path_args = \explode('/', $request_path);
      $prefix = \array_shift($path_args);

      if ($prefix) {
        // Search prefix within added languages.
        if (isset($languages[$prefix]) && $request->hasSession()) {
          // Remember the users language once set. This will be the default if
          // directed to a URL with no language prefix like in a hard coded link.
          $request->getSession()->set('language_preference', $languages[$prefix]->getId());
          return $languages[$prefix];
        }
      }
    }
    if ($request->hasSession() && $preference = $request->getSession()->get('language_preference')) {
      return $languages[$preference];
    }

    return $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackCandidates(array &$candidates = [], array $context = []) {
    $entity = $context['data'];
    // The first candidate should always be the desired language if
    // specified.
    if (!empty($context['langcode'])) {
      $candidates = [$context['langcode'] => $context['langcode']] + $candidates;
    }
    if (\str_contains($context['langcode'], '-') && $entity instanceof EntityInterface && $this->entityVariationHandler->variantHandler->hasEnabledVariations($entity)) {
      $variant = $this->findVariant($context['langcode'], $entity);
      if ($variant) {
        $variation_code = $this->entityVariationHandler->generateVariationCode($variant->language_code, $entity, $variant);
        if ($variation_code) {
          $candidates = [$variation_code => $variation_code] + $candidates;
        }
      }
    }
  }

  /**
   * Find Variant.
   *
   * @param string $resolve_langcode
   *   The langcode code.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\variants\Entity\Variant|null
   *   The variant entity or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function findVariant(string $resolve_langcode, EntityInterface $entity): ?Variant {
    static $variant_ids = [];
    $already_resolved_variants_id = "$resolve_langcode--{$entity->getEntityTypeId()}--{$entity->id()}";
    if (!empty($variant_ids[$already_resolved_variants_id])) {
      $variant = $this->entityVariationHandler->entityTypeManager->getStorage('variant')->load($variant_ids[$already_resolved_variants_id]['variant_id']);
      $variant->language_code = $variant_ids[$already_resolved_variants_id]['language_code'];
      return $variant;
    }

    $code_info = LocationHandler::parseCode($resolve_langcode);
    $langcode = $code_info['langcode'];
    $location_code = $code_info['location'];
    /** @var \Drupal\variants\Entity\Variant[] $variant */
    // Find a Variant for this location.
    $variants = $this->entityVariationHandler->entityTypeManager->getStorage('variant')->loadByProperties([
      'type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'locations' => $location_code,
    ]);
    $variant = NULL;
    if (!empty($variants)) {
      $variant = $variants[\array_key_first($variants)];
    }
    if ($variant === NULL) {
      // Fallback to a less specific location.
      $location = $this->entityVariationHandler->locationHandler->getLocation($location_code);
      if ($location !== NULL) {
        $parent = $location->getParent()->getId();
        $variant = $this->findVariant("$langcode-$parent", $entity);
      }
    }
    if ($variant) {
      $variation_code = $this->entityVariationHandler->generateVariationCode($langcode, $entity, $variant);
      if (!$entity->hasTranslation($variation_code)) {
        if ($langcode != LocationHandler::DEFAULT_LANGUAGE) {
          // Location found, but no translation. Use the default translation for
          // this location.
          $langcode = LocationHandler::DEFAULT_LANGUAGE;
          $variant = $this->findVariant("$langcode-$location_code", $entity);
        }
        else {
          // There may be a translation for this location, but no english
          // localization which is what was requested. Fall back to the default.
          $variant = $this->findVariant(LocationHandler::DEFAULT_LANGUAGE . '-' . LocationHandler::DEFAULT_LOCATION, $entity);
        }
      }
      // Pass along the language that was resolved to.
      $variant->language_code = $langcode;
    }
    if (!empty($variant)) {
      $variant_ids[$already_resolved_variants_id] = [
        'variant_id' => $variant->id(),
        'language_code' => $langcode,
      ];
    }
    return $variant;
  }

}
