<?php

namespace Drupal\string_location_variant;

use Drupal\variants\Entity\Variant;
use Drupal\variants\VariantHandler;
use Drupal\Core\Database\Connection;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\string_location_variant\Entity\Strings;
use Drupal\location_variant\EntityVariationHandler;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workspaces\Plugin\Validation\Constraint\EntityWorkspaceConflictConstraint;

/**
 * Strings Repository service.
 */
class StringsRepository {

  use StringTranslationTrait;

  protected Connection $connection;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected EntityRepositoryInterface $entityRepository;
  protected EntityVariationHandler $variationHandler;
  protected LocationHandler $locationHandler;
  protected VariantHandler $variantHandler;
  private ConfigFactoryInterface $configFactory;
  private $strings = [];

  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, EntityVariationHandler $location_variant_entity_handler, LocationHandler $location_variant_location_handler, VariantHandler $variant_handler, ConfigFactoryInterface $config_factory) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->variationHandler = $location_variant_entity_handler;
    $this->locationHandler = $location_variant_location_handler;
    $this->variantHandler = $variant_handler;
    $this->configFactory = $config_factory;
  }

  /**
   * Save translation.
   *
   * @param array $values
   *   An array of translation values.
   *   - source: The string as it appears in t('This string right here.').
   *   - translation: The translation string that will replace the source.
   *   - context: The TranslatableMarkup context.
   *   - language: The langcode that this translation applies to.
   *
   * @return \Drupal\string_location_variant\Entity\Strings|null
   *   The updated string or NULL if the variant limit has been reached.
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function saveTranslation(array $values): ?Strings {
    $translation = $this->getStringTranslation($values['source'], $values['language']);

    if ($translation) {
      // Run the workspace conflict validation constraint before saving so we
      // can stop the process and display a message to the user that the entity
      // is already being edited in another workspace and can't be saved.
      foreach ($translation->validate()->getEntityViolations() as $violation) {
        if ($violation->getConstraint() instanceof EntityWorkspaceConflictConstraint) {
          \Drupal::messenger()->addWarning($violation->getMessage());
          return NULL;
        }
      }
    }

    if (empty($translation)) {
      $translation = $this->createStringTranslation($values);
      $translation->_justCreated = TRUE;
    }

    $translation_to_save_variation_code = LocationHandler::parseCode($values['language']);
    $translation_variation_code = LocationHandler::parseCode($translation->language()->getId());
    $variant = $translation->get('variants')->get($translation_variation_code['location'])->get('entity')->getTarget()->getValue();
    $location_ids = $variant->getLocationIds();
    $translation_is_correct_localization = in_array($translation_to_save_variation_code['location'], $location_ids);

    // Is this a new translation or a new localization or should we skip and
    // update the current translation?
    if ($translation_to_save_variation_code['langcode'] !== $translation_variation_code['langcode'] && $translation_is_correct_localization) {
      $translation = $this->addTranslation($values, $translation, $variant);
    } elseif (!$translation_is_correct_localization) {
      if ($this->variantLimitReached($translation)) {
        return NULL;
      }
      $translation = $this->addLocalization($values, $translation);
    }

    // Save the new values.
    $translation->set('label', $values['translation']);
    $translation->set('source', $values['source']);
    $translation->set('context', $values['context']);
    $translation->save();

    return $translation;
  }

  /**
   * Get string translation.
   *
   * @param string $source
   *   The source string as it appears in t('This string right here.').
   * @param $language
   *   The translation language. Uses the variant fallback logic to resolve to
   *   the most relevant localization and translation.
   *
   * @return \Drupal\string_location_variant\Entity\Strings|null
   *   The string translation or null.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStringTranslation(string $source, $language): ?Strings {
    $processed = &drupal_static(__function__);
    $translation = NULL;
    // Check if we have a translation for this source.
    if (empty($processed[$source][$language]) && \Drupal::database()->schema()->tableExists('string')) {
      $processed[$source][$language] = TRUE;
      $strings = $this->entityTypeManager->getStorage('string')->loadByProperties([
        'source' => $source,
      ]);
      if (!empty($strings)) {
        $string = reset($strings);
        $translation = $processed[$source][$language] =  $this->entityRepository->getTranslationFromContext($string, $language);
      }
    } elseif (!empty($processed[$source][$language]) && $processed[$source][$language] instanceof Strings) {
      // If we've already translated this source, return the translation.
      return $processed[$source][$language];
    }

    return $translation;
  }

  /**
   * Create string translation.
   *
   * @param array $values
   *   An array of string values.
   * @see \Drupal\string_location_variant\StringsRepository::saveTranslation.
   *
   * @return \Drupal\string_location_variant\Entity\Strings
   *   The new string.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createStringTranslation(array $values) {
    $string = Strings::create([
      'source'=> $values['source'],
      'label' => $values['source'],
      'context' => $values['context'],
    ]);
    $string->save();

    return $string;
  }

  /**
   * Add localization.
   *
   * @param array $values
   *   An array of string values.
   * @see \Drupal\string_location_variant\StringsRepository::saveTranslation.
   * @param \Drupal\string_location_variant\Entity\Strings $string
   *   The String to add a localized version to.
   *
   * @return \Drupal\string_location_variant\Entity\Strings
   *   The localized revision.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addLocalization(array $values, Strings $string) {
    $code = LocationHandler::parseCode($values['language']);
    // An english localization is required as a fallback before adding a
    // translation.
    $localization_variation_code = $this->variationHandler->generateVariationCode(LocationHandler::DEFAULT_LANGUAGE, $string);
    $this->variantHandler->prepareVariation($string, LocationHandler::DEFAULT_VARIATION_CODE, $localization_variation_code);
    $locations = [$code['location'] => $this->locationHandler->getLocation($code['location'])];
    $variant = $this->variationHandler->createVariant($string, $locations);

    // Add a translation if that is what is being saved or return the english
    // localization.
    if ($code['langcode'] === LocationHandler::DEFAULT_LANGUAGE) {
      return $string->getTranslation($localization_variation_code);
    }
    return $this->addTranslation($values, $string, $variant);
  }

  /**
   * Add translation.
   *
   * @param array $values
   *   An array of string values.
   * @see \Drupal\string_location_variant\StringsRepository::saveTranslation.
   * @param \Drupal\string_location_variant\Entity\Strings $string
   *   The string to add a translation to.
   * @param \Drupal\variants\Entity\Variant|NULL $variant
   *   The variant (localization) this translation applies to.
   *
   * @return \Drupal\string_location_variant\Entity\Strings
   */
  public function addTranslation(array $values, Strings $string, Variant $variant = NULL) {
    $code = LocationHandler::parseCode($values['language']);
    $translation_variation_code = $this->variationHandler->generateVariationCode($code['langcode'], $string, $variant);
    $this->variantHandler->prepareVariation($string, LocationHandler::DEFAULT_VARIATION_CODE, $translation_variation_code);

    return $string->getTranslation($translation_variation_code);
  }

  /**
   * Variant limit reached.
   *
   * @param $translation
   *   The strings entity to check.
   *
   * @return bool
   *   Whether or not the variant limit has been reached.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function variantLimitReached($translation) {
    $variants = $this->variationHandler->getVariants($translation);
    $max_variants = $this->configFactory->get('variants.settings')->get('variant_limit');
    if (count($variants) >= $max_variants) {
      return TRUE;
    }
    return FALSE;
  }

}
