<?php

namespace Drupal\string_location_variant;

use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a string entity type.
 */
interface StringInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
