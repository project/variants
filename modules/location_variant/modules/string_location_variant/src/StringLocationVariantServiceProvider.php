<?php

namespace Drupal\string_location_variant;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Defines a service provider for the String Location Variant module.
 */
class StringLocationVariantServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('string_translation')->setClass('Drupal\string_location_variant\StringTranslationManager');
  }

}
