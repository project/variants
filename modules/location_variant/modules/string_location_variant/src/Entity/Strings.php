<?php

namespace Drupal\string_location_variant\Entity;

use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\string_location_variant\StringInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;

/**
 * Defines the string entity class.
 *
 * @ContentEntityType(
 *   id = "string",
 *   label = @Translation("String"),
 *   label_collection = @Translation("Strings"),
 *   label_singular = @Translation("string"),
 *   label_plural = @Translation("strings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count string",
 *     plural = "@count strings",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\string_location_variant\StringListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\string_location_variant\StringAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\string_location_variant\Form\StringForm",
 *       "edit" = "Drupal\string_location_variant\Form\StringForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\string_location_variant\Routing\StringHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "string",
 *   data_table = "string_field_data",
 *   revision_table = "string_revision",
 *   revision_data_table = "string_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer string",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/string",
 *     "add-form" = "/string/add",
 *     "canonical" = "/string/{string}",
 *     "edit-form" = "/string/{string}/edit",
 *     "delete-form" = "/string/{string}/delete",
 *   },
 *   field_ui_base_route = "entity.string.settings",
 * )
 */
class Strings extends RevisionableContentEntityBase implements StringInterface, EntityPublishedInterface {

  use EntityPublishedTrait;
  use EntityChangedTrait;
  use EntityOwnerTrait;


  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // The source field is read only and once set can never be changed.
    if (!$this->isNew()) {
      $original_source = $storage->loadUnchanged($this->id())->get('source')->value;
      $this->set('source', $original_source);
    }
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  public function getTitle() {
    return $this->get('label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['source'] = BaseFieldDefinition::create('string_long')
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel('Source')
      ->setRequired(TRUE)
      ->addConstraint('UniqueSource')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['label'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel('Translation')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['context'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel('Context')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel('Author')
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel('Authored on')
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the string was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel('Changed')
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the string was last edited.'));

    return $fields;
  }

}
