<?php

namespace Drupal\string_location_variant;

use Drupal\Core\Database\Database;
use Drupal\bootstrap\Utility\Crypt;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Database\Query\Upsert;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;

/**
 * Remembers the strings that were rendered on this page.
 */
class StringTranslationManager extends TranslationManager {

  const StaticCache = 'StringTranslationManager::cache';

  private Upsert $upsert;

  /**
   * Translate string.
   *
   * Aggregates a list of translated strings that were used to build this page.
   * It then passes the list to the front end via drupalSettings.
   *
   * @see \Drupal\string_location_variant\EventSubscriber\TwigRenderTemplateSubscriber
   * @see string-translation-mode.js
   */
  public function translateString(TranslatableMarkup $translated_string) {
    $request = \Drupal::request();
    $translation = parent::translateString($translated_string);
    if (!$request->hasSession()) {
      return $translation;
    }

    // Build a list of strings as users navigate the site.
    $string_collection_mode_enabled = \Drupal::config('string_location_variant.settings')->get('string_collection');
    if ($string_collection_mode_enabled && !\Drupal::service('router.admin_context')->isAdminRoute()) {
      $string = $translated_string->getUntranslatedString();
      self::stringCollection([
        'id' => Crypt::hashBase64($string),
        'label' => $string,
        'context' => $translated_string->getOptions()['context'] ?? NULL,
      ]);
    }

    $session = $request->getSession();
    // Translate strings mode is enabled. This is what triggers TwigRenderTemplateSubscriber.
    if (!empty($session) && $session->isStarted() && $session->get('string_translation_mode')) {
      $options = $translated_string->getOptions();
      $arguments = $translated_string->getArguments();
      // We need the placeholder replaced string to search for it on the page, so
      // lets access the protected placeholderFormat() method.
      $formattable_markup = new \ReflectionMethod(FormattableMarkup::class, 'placeholderFormat');
      $formattable_markup->setAccessible(TRUE);
      $translation_for_replacement = $translation;
      // Address plural strings.
      if ($translated_string instanceof PluralTranslatableMarkup) {
        $arguments['@count'] = $this->getCount($translated_string);
        $translated_array = explode(PoItem::DELIMITER, $translation);
        $translation_for_replacement = $arguments['@count'] === 1 ? $translated_array[0] : $translated_array[1];
      }
      $replaced = $formattable_markup->invoke(new FormattableMarkup($translation_for_replacement, $arguments), $translation_for_replacement, $arguments);
      $source = $translated_string->getUntranslatedString();

      self::rememberString($source, $translation, $replaced, $options['context'] ?? '', $arguments);

      // Remove translated arguments of strings. Nested translations aren't
      // supported by our UI.
      $strings = self::getStrings();
      foreach ($arguments as $argument) {
        $argument_string = $argument instanceof MarkupInterface ? $argument->__toString() : $argument;
        if (!empty($strings[$argument_string])) {
          unset($strings[$argument_string]);
          self::rememberStrings($strings);
        }
      }

      if (!empty($options['context'])) {
        // Pass along the context as part of the string text since we can't wrap
        // strings in markup to include is as an attribute. The markup could be
        // passed as strings to things like the twig link function. This will be
        // parsed out on page load in string-translation-mode.js.
        $translation = $translation . "[[{$options['context']}]]";
      }
    }
    return $translation;
  }

  /**
   * Aggregates a list of translated strings that were used to build this page.
   *
   * @return array|mixed
   *   An array of translatable strings.
   */
  public static function rememberString(string $source = NULL, string $translation = NULL, string $replaced = NULL, string $context = NULL, $arguments = NULL) {
    $translations = &drupal_static(self::StaticCache);
    if (!isset($translations)) {
      $translations = [];
    }
    if ($translation) {
      $translations[$translation][$context] = [
        'source' => $source,
        'replaced' => $replaced,
        'args' => $arguments
      ];
    }

    return $translations;
  }

  /**
   * Remember strings.
   *
   * Allows overriding the strings cache.
   *
   * @param array $strings
   *   An array of translatable strings.
   *
   * @return void
   */
  public static function rememberStrings(array $strings) {
    $translations = &drupal_static(self::StaticCache);
    $translations = $strings;
  }

  /**
   * Get strings.
   *
   * @return array
   *   An array of translatable strings.
   */
  public static function getStrings (): array {
    $translations = &drupal_static(self::StaticCache);
    if (!isset($translations)) {
      $translations = [];
    }
    return $translations;
  }

  /**
   * Get count.
   *
   * @param \Drupal\Core\StringTranslation\PluralTranslatableMarkup $plural_translatable_markup
   *   Plural translatable markup.
   *
   * @return mixed
   *   The count of this plural translatable markup.
   */
  protected function getCount(PluralTranslatableMarkup $plural_translatable_markup) {
    $reflection = new \ReflectionClass($plural_translatable_markup);
    $property = $reflection->getProperty('count');
    $property->setAccessible(TRUE);
    return $property->getValue($plural_translatable_markup);
  }

  /**
   * String collection.
   *
   * Cache a list of strings used on the site as users navigate.
   * @see \Drupal\string_location_variant\EventSubscriber\StringCollectionSubscriber
   *
   * @return array
   *   An upsert string_collection instance.
   */
  public function stringCollection($values = NULL): ?Upsert {
    if (!isset($this->upsert) && !empty($values)) {
      $this->upsert = Database::getConnection()->upsert('string_collection')->fields(['id', 'label', 'context'])->key('id');
    }
    if ($values) {
      $this->upsert->values($values);
    }
    return $this->upsert ?? NULL;
  }

}
