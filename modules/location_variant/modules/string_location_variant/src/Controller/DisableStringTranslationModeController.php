<?php

namespace Drupal\string_location_variant\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DisableStringTranslationModeController extends ControllerBase {

  /**
   * Turns off string translation mode.
   */
  public function build(Request $request) {
    $request->getSession()->remove('string_translation_mode');
    $this->messenger()->deleteAll();
    return new RedirectResponse($request->query->get('destination') ?? '/');
  }

}
