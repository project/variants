<?php

namespace Drupal\string_location_variant\Event;

use Drupal\Component\EventDispatcher\Event;

class StringsUiIgnoreElementsEvent extends Event {

  protected array $ignoredElements = [];

  /**
   * @return array
   */
  public function getIgnoredElements(): array {
    return $this->ignoredElements;
  }

  /**
   * @param array $ignoredElements
   */
  public function addIgnoredElements(array $ignoredElements): void {
    $this->ignoredElements = array_merge($ignoredElements, $this->ignoredElements);
  }

}
