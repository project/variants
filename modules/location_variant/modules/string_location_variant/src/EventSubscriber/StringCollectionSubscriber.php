<?php

declare(strict_types=1);

namespace Drupal\string_location_variant\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\string_location_variant\StringTranslationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

final class StringCollectionSubscriber implements EventSubscriberInterface {

  public function __construct(
    protected TranslationInterface $stringTranslationManager,
  ) {}

  /**
   * Kernel response event handler.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    $upsert = $this->stringTranslationManager->stringCollection();
    if (!empty($upsert)) {
      // Upsert all the strings that were collected during this response.
      // @see StringTranslationManager->translateString().
      $upsert->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      // Run last.
      KernelEvents::RESPONSE => ['onKernelResponse', -999999999999999],
    ];
  }

}
