<?php

namespace Drupal\string_location_variant\EventSubscriber;

use Drupal\string_location_variant\Event\StringsUiIgnoreElementsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Strings UI ignored elements.
 */
class StringsUiIgnoredElements implements EventSubscriberInterface {

  /**
   * On ignore elements.
   *
   * Excludes some items from being highlighted by the strings UI.
   *
   * @param \Drupal\string_location_variant\Event\StringsUiIgnoreElementsEvent $event
   *
   * @return void
   */
  public function onIgnoreElements(StringsUiIgnoreElementsEvent $event) {
    $event->addIgnoredElements([
      '#toolbar-administration',
      'nav.tabs',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      StringsUiIgnoreElementsEvent::class => ['onIgnoreElements'],
    ];
  }

}
