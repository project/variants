<?php

namespace Drupal\string_location_variant\EventSubscriber;

use Drupal\twig_events\Event\TwigRenderTemplateEvent;
use Drupal\string_location_variant\StringTranslationManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * String Location Variant event subscriber.
 */
class TwigRenderTemplateSubscriber implements EventSubscriberInterface {

  public function onTwigRenderTemplate(TwigRenderTemplateEvent $event) {
    $strings = StringTranslationManager::getStrings();
    if (!empty($strings)) {
      $bubbleable = [];
      foreach ($strings as $string => $contexts) {
        foreach ($contexts as $context => $values) {
          $bubbleable['#attached']['drupalSettings']['stringLocationVariant'][$string][$context] = $values;
        }
      }
      drupal_static_reset('rememberString');
      $bubbleable['#attached']['drupalSettings']['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $bubbleable['#attached']['library'][] = 'string_location_variant/string_translation_mode';
      \Drupal::service('renderer')->render($bubbleable);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      TwigRenderTemplateEvent::class => ['onTwigRenderTemplate'],
    ];
  }

}
