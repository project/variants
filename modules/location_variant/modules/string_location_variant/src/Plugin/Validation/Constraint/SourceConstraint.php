<?php

namespace Drupal\string_location_variant\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Ensures that source strings are not duplicated.
 * @Constraint(
 *   id = "UniqueSource",
 *   label = @Translation("Unique Source", context = "Validation"),
 *   type = "string"
 * )
 */
class SourceConstraint extends Constraint {

  // The message that will be shown if the value already exists.
  public $notUnique = 'The source string %value already <a href="@url">exists</a>.';

}
