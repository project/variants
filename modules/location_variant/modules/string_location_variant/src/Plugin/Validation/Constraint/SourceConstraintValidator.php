<?php

namespace Drupal\string_location_variant\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

class SourceConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  public function validate($value, Constraint $constraint) {
    foreach($value as $item) {
      $source = $item->value;
      // There can be only one. https://en.wikipedia.org/wiki/Highlander_(franchise)
      if (!empty($source) && $value->getEntity()->isNew()) {
        $strings = $this->entityTypeManager->getStorage('string')->loadByProperties([
          'source' => $source,
        ]);

        if (!empty($strings)) {
          $string = reset($strings);
          $this->context->addViolation($constraint->notUnique, [
            '%value' => $source,
            '@url' => $string->toUrl('edit-form')->toString(),
            ]);
        }
      }
    }
  }


}
