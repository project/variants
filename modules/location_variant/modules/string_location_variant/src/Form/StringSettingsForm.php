<?php

namespace Drupal\string_location_variant\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for a string entity type.
 */
class StringSettingsForm extends FormBase {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
    );
  }

  public function __construct(
    protected $database,
    protected $entityTypeManager,
  ) {}


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'string_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('workspace_safe', TRUE);
    $config = $this->config('string_location_variant.settings');
    $string_collection_enabled = $config->get('string_collection');
    $form['string_collection_details'] = [
      '#type' => 'details',
      '#title' => $this->t('String Collection'),
      '#open' => $string_collection_enabled,
    ];
    $form['string_collection_details']['string_collection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable String Collection'),
      '#description' => $this->t('String collection mode builds a list of all strings that pass through the translation system as you navigate through the site. <strong>Do not enable in a production environment!</strong>'),
      '#default_value' => $string_collection_enabled,
      '#group' => 'string_collection_details',
    ];
    $options = [];
    if ($string_collection_enabled) {
      $options = $this->database->select('string_collection', 'sc')->fields('sc', ['id', 'label', 'context'])->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);
      if (!empty($options)) {
        $strings = $this->getStringEntitiesInfo();
        if (!empty($strings)) {
          foreach($options as $id => &$option) {
            unset($option['id']);
            if (is_null($option['context'])) {
              $option['context'] = '';
            }
            $option['exists'] = array_key_exists($option['label'], $strings) ? 'Exists' : '';
          }
        }
      }
    }
    if ($string_collection_enabled && !empty($options)) {
      $form['string_collection_details']['create_strings'] = [
        '#type' => 'submit',
        '#value' => $this->t("Create String entities from collection"),
        '#submit' => [[$this, 'createStringsFromCollection']],
        '#group' => 'string_collection_details',
      ];
      $form['string_collection_details']['collected_strings'] = [
        '#type' => 'tableselect',
        '#caption' => $this->formatPlural(count($options), '@count string has been collected.', '@count strings have been collected.'),
        '#header' => [
          'label' => $this->t('Label'),
          'context' => $this->t('Context'),
          'exists' => $this->t('String entity exists'),
        ],
        '#options' => $options ?? [],
        '#empty' => $this->t('Enable string collection mode and navigate around the site to start collecting strings!'),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Get string entities info.
   *
   * @return array
   *   An array containing all the labels and context of all string entities.
   */
  protected function getStringEntitiesInfo(): array{
    $string_storage = $this->entityTypeManager->getStorage('string');
    $string_entity_ids = $string_storage->getQuery()->accessCheck(FALSE)->execute();
    return $this->database->select('string_field_revision', 'sfr')->distinct()->fields('sfr', ['label', 'context'])->condition('revision_id', array_flip($string_entity_ids), 'IN')->execute()->fetchAllKeyed();
  }

  /**
   * Create strings from collection.
   */
  public function createStringsFromCollection(array &$form, FormStateInterface $form_state) {
    $collected_string_ids = array_filter($form_state->getValue('collected_strings'));
    if (!empty($collected_string_ids)) {
      $collected_strings = $this->database->select('string_collection', 'sc')->fields('sc', ['id', 'label', 'context'])->condition('id', $collected_string_ids, 'IN')->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);
      $chunks = array_chunk($collected_strings, 15);
      $operations = [];
      foreach ($chunks as $chunk) {
        $operations[] = [
          [$this, 'createStrings'],
          [$chunk],
        ];
      }
      batch_set([
        'title' => $this->t('Creating string entities...'),
        'operations' => $operations,
        'finished' => [$this, 'finished'],
      ]);
    } else {
      $this->messenger()->addStatus($this->t('No collected strings selected to create string entities.'));
    }
  }

  /**
   * Create strings batch process.
   */
  public function createStrings(array $chunk, &$context) {
    $storage = $this->entityTypeManager->getStorage('string');
    foreach ($chunk as $collected_string) {
      $values = ['label' => $collected_string['label'], 'context' => $collected_string['context']];
      $values = array_filter($values);
      $exists = $storage->loadByProperties($values);
      if (empty($exists)) {
        $storage->create($values)->save();
        $context['results']['count']++;
      }
    }
  }

  /**
   * Batch finished callback function.
   */
  public function finished($success, $results, $operations) {
    if ($success) {
      $this->messenger()->addStatus($this->formatPlural($results['count'], 'Created @count string entity.', 'Created @count string entities.'));
    }
    else {
      $this->messenger()->addError($this->t('Unable to create '));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('string_location_variant.settings');
    $config->set('string_collection', $form_state->getValue('string_collection'));
    $config->save();

    $this->messenger()->addStatus($this->t('The configuration has been updated.'));
  }

}
