<?php

namespace Drupal\string_location_variant\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Component\Gettext\PoItem;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\location_variant\LocationHandler;

/**
 * Provides a String Location Variant form.
 */
class TranslateStringForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'string_location_variant_translate_string';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('workspace_safe', TRUE);

    $translation = $this->getRequest()->request->get('translation');
    $language = $this->getRequest()->request->get('language');
    $context = $this->getRequest()->request->get('context');
    $args = $this->getRequest()->request->get('args');
    if (!empty($args)) {
      $arguments = Json::decode($args);
    }
    $source = $form_state->getUserInput()['source'] ?? $this->getRequest()->request->get('source');

    $form['source'] = [
      '#type' => 'hidden',
      '#value' => $source,
    ];
    $form['context'] = [
      '#type' => 'hidden',
      '#value' => $form_state->getUserInput()['context'] ?? $context,
    ];
    $form['language'] = [
      '#type' => 'hidden',
      '#value' => $form_state->getUserInput()['language'] ?? $language,
    ];

    $translated_array = explode(PoItem::DELIMITER, $translation);
    $plural = str_contains($source, PoItem::DELIMITER);
    $length = strlen($translation);
    $form['translation'] = [
      '#type' => $length < 128 ? 'textfield' : 'textarea',
      '#size' => strlen($translation),
      '#default_value' => $plural ? $translated_array[0] : $translation,
      '#context' => $this->getRequest()->request->get('context'),
    ];

    if ($plural) {
      $form['translation']['#title'] = 'Singular';
      $plural_value = $form_state->getUserInput()['plural'] ?? $translated_array[1];
      $plural_length = strlen($translation);
      $form['plural'] = [
        '#type' => $plural_length < 128 ? 'textfield' : 'textarea',
        '#title' => 'Plural',
        '#default_value' => $plural_value,
        '#context' => $this->getRequest()->request->get('context'),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $trigger = $form_state->getTriggeringElement();
    if (!empty($trigger['#name']) && $trigger['#name'] === 'op') {
      $language = $form_state->getValue('language');
      if ($language === LocationHandler::DEFAULT_LANGUAGE) {
        $form_state->setError($form['translation'], 'Please provide a full langcode prefix in the URL when translating strings. e.g. en-US, ko-KR, fr-CA, pt-BR.');
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($values['plural'])) {
      $values['translation'] = $values['translation'] . PoItem::DELIMITER . $values['plural'];
    }

    $strings_repository = \Drupal::service('strings.repository');
    $translation = $strings_repository->saveTranslation([
      'source' => $values['source'],
      'translation' => $values['translation'],
      'context' => $values['context'],
      'language' => $values['language'],
    ]);
    if ($translation) {
      $code = LocationHandler::parseCode($values['language']);
      $message = [
        'status' => [
          '#markup' => $this->t('@action a %langcode @type.', [
            '@action' => isset($translation->_justCreated) ? 'Added' : 'Updated',
            '%langcode' => $values['language'],
            '@type' => $code['langcode'] === LocationHandler::DEFAULT_LANGUAGE ? 'localization' : 'translation',
          ]),
        ],
        'string' => [
          '#theme' => 'item_list',
          '#items' => [$values['translation']],
        ],
      ];
      $this->messenger()->addStatus($message);
    }
    elseif ($translation === NULL) {
      $this->messenger()->addStatus('This entity already has maximum localizations which is the <a href="@settings_url">current maximum</a>.', [
          '@settings_url' => Url::fromRoute('variants_settings')->toString(),
        ]);
    }
    $form_state->setRedirectUrl(Url::fromUserInput($this->getRequest()->query->get('destination')));
  }

}
