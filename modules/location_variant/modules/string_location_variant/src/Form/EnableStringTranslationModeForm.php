<?php

namespace Drupal\string_location_variant\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * A form for enabling string translation mode.
 */
class EnableStringTranslationModeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'enable_string_translation_mode';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('workspace_safe', TRUE);
    $form['#title'] = $this->t('Enable String Translation Mode');

    $form['#attributes']['class'][] = 'confirmation';
    $form['instructions'] = [
      'intro' => ['#markup' => $this->t('You about to enter String Translation Mode. Click "<em>Disable String Translation Mode</em>" in the admin menu to exit. You can manage the strings translations on the <a href="@admin_url" style="color: lightblue;">administration page</a>.', ['@admin_url' => Url::fromRoute('entity.string.collection')->toString()])],
      'queue for export' => [
        '#theme' => 'item_list',
        '#title' => t('Queue strings for translation export'),
        '#items' => [
          ['#markup' => 'Click a highlighted string and then click the + button.'],
          ['#markup' => 'The string should now show up in your workspace.'],
          ['#markup' => 'Once this item has been transitioned through your content workflow it can be exported and translated.'],
        ],
      ],
      'directions' => [
        '#theme' => 'item_list',
        '#title' => t('Langcode URL prefixes:'),
        '#items' => [
          ['#markup' => 'The langcode URL prefix determines if you are creating a Translation, a Localization, or a Localized Translation.'],
          ['#markup' => 'Use a langcode with the global location to add a translation. e.g. <em>fr-global</em>, <em>de-global</em>, <em>ko-global</em>'],
          ['#markup' => 'Use a specific location to add a localization. e.g. <em>en-US</em>, <em>en-CA</em>, <em>en-MX</em>'],
          ['#markup' => 'Use a langcode with a location to add a localized translation. e.g. <em>pt-BR</em>, <em>fr-CA</em>, <em>de-DE</em>'],
        ],
      ],
    ];
    $form['confirm'] = ['#type' => 'hidden', '#value' => 1];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enable'),
      '#button_type' => 'primary',
    ];

    // Prepare cancel link.
    $query = $this->getRequest()->query;
    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      $form['actions']['cancel'] = [
        '#type' => 'link',
        '#title' => $this->t('cancel'),
        '#attributes' => ['class' => ['button']],
        '#url' => Url::fromUserInput('/' . ltrim($options['path'], '/'), $options),
        '#cache' => [
          'contexts' => [
            'url.query_args:destination',
          ],
        ],
      ];
    }

    // By default, render the form using theme_confirm_form().
    if (!isset($form['#theme'])) {
      $form['#theme'] = 'confirm_form';
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $request = $this->getRequest();
    $request->getSession()->set('string_translation_mode', TRUE);
    return new RedirectResponse($request->query->get('destination') ?? '/');
  }

}
