<?php

namespace Drupal\string_location_variant\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for the string entity edit forms.
 */
class StringForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    if (!empty($form['source']['widget'][0]['value']['#default_value'])) {
      $form['source']['widget'][0]['value']['#disabled'] = TRUE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New string %label has been created.', $message_arguments));
        $this->logger('string_location_variant')->notice('Created new string %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The string %label has been updated.', $message_arguments));
        $this->logger('string_location_variant')->notice('Updated string %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.string.collection');

    return $result;
  }

}
