<?php

namespace Drupal\string_location_variant;

use Drupal\Core\DestructableInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

class StringsTranslator implements TranslatorInterface, DestructableInterface {

  use DependencySerializationTrait;

  /**
   * Cached translations.
   *
   * @var array
   *   Array of \Drupal\locale\LocaleLookup objects indexed by language code
   *   and context.
   */
  protected $translations = [];


  /**
   * {@inheritdoc}
   */
  public function reset() {
    unset($this->translateEnglish);
    $this->translations = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    // If the language is not suitable for this module, just return.
    if ($langcode == LanguageInterface::LANGCODE_SYSTEM) {
      return FALSE;
    }
    $current_language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $translation = \Drupal::service('strings.repository')->getStringTranslation($string, $current_language);
    if ($translation && $label = $translation->label()) {
      return $label;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    foreach ($this->translations as $context) {
      foreach ($context as $lookup) {
        if ($lookup instanceof DestructableInterface) {
          $lookup->destruct();
        }
      }
    }
  }

}
