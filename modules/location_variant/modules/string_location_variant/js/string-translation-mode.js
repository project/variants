(function ($, Drupal, once) {

  /**
   * Highlight all translatable strings by wrapping the string in a web
   * component which when clicked loads an inline form to translate the string.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.stringLocationVariant = {
    attach: function (context) {
      if (drupalSettings.stringLocationVariantEnabled) {
        once('stringsHighlighted', 'body', context).forEach(function (element) {
          Drupal.behaviors.stringLocationVariant.findAndReplace(element);
        });
        once('languageOptioned', '#toolbar-item-translate-strings-tray select', context).forEach(function (select) {
          select.addEventListener('change', function (e) {
            e.preventDefault();
            window.location = '/' + e.target.value + '/' + drupalSettings.path.currentPath;
          });
        });
      }
    },

    /**
     * Find and replace.
     *
     * Find strings on the page and replace them with our highlighter web
     * component.
     *
     * Why not just do something like
     * document.body.replace(translatableString, webComponent)?
     *
     * HTML is complex and using only RegEx to replace translated
     * strings leads to messed up values in attributes which breaks things. Let's
     * loop through the DOM and compare only the text nodes to our list of
     * string replacements to figure out the best replacement candidates.
     *
     * @param parent
     *   The element to traverse.
     */
    findAndReplace: function (parent) {
      // Recursively loop through every text node in the body tag once.
      [].slice.call(parent.childNodes, 0).forEach(function(child) {
        if (child.nodeType === Node.TEXT_NODE) {
          // Ignore script tags. Sometimes they have the strings we are looking
          // for in JSON.
          if (child.parentElement && child.parentElement.nodeName === 'SCRIPT') {
            return;
          }

          // See if this node is a child of an element we should ignore like
          // the administration menu.
          for (let ignoredElement in drupalSettings.stringLocationVariantIgnoreElements) {
            if (child.parentNode.closest(drupalSettings.stringLocationVariantIgnoreElements[ignoredElement])) {
              return;
            }
          }

          // Check if any translatable string's should be substituted in this
          // text node.
          const textNode = child;
          let replacementCandidate = [];

          for (let translatableString in drupalSettings.stringLocationVariant) {
            const data = drupalSettings.stringLocationVariant[translatableString];
            const context = Object.keys(data)[0];
            const replaced = data[context].replaced;
            const args = data[context].args;

            let search = replaced;
            if (context) {
              // Include context in the search so we can replace it.
              // @see StringTranslationManager.
              search = replaced + "[[" + context + "]]";
            }

            // Create the web component element for use as the string replacement.
            const component = document.createElement('translatable-string');
            component.innerHTML = replaced;
            const escapedHtmlEntitiesSearch = component.innerHTML;
            component.dataset.translation = translatableString.replace(/'/g, "&apos;");
            component.dataset.source = data[context].source.replace(/'/g, "&apos;");
            component.dataset.active = 'false';
            if (args) {
              component.dataset.args = JSON.stringify(args);
            }
            if (context) {
              component.dataset.context = context;
            }
            // Replace the escaped quotes with ticks for proper HTML.
            let replacement = component.outerHTML.replace(/\"/g, '\'');
            if (textNode.nodeValue.includes(search) && textNode.parentNode) {
              // The translatable string is included in this textNode.nodeValue which
              // means we've found an exact replacement. It may not be the best one
              // though so let's add it as a replacement candidate.
              let nextNode = textNode;
              let currentNode;
              // Find the closest element to this text node.
              do {
                currentNode = nextNode;
                nextNode = nextNode.parentNode;
              } while (typeof currentNode.innerHTML === 'undefined');
              replacementCandidate.push({
                node: currentNode,
                search: search,
                escapedHtmlEntitiesSearch: escapedHtmlEntitiesSearch,
                replacement: replacement,
                translatableString: translatableString,
              });
            }
            // Check to see if this is a substring of a longer translated string.
            // The string can be spread over multiple text nodes.
            else if (search.includes(textNode.nodeValue)) {
              // Expand the text nodes we are looking at by going up the chain of
              // parents to see if the translatable string is within. When the
              // translatable string is no longer contained within, that element
              // is the closest string containing element to replace.
              let currentParent = textNode.parentNode;
              while (
                currentParent &&
                currentParent.parentNode &&
                search.includes(currentParent.outerHTML)
              ) {
                currentParent = currentParent.parentNode;
              }
              if (currentParent) {
                replacementCandidate.push({
                  node: currentParent,
                  search: search,
                  escapedHtmlEntitiesSearch: escapedHtmlEntitiesSearch,
                  replacement: replacement,
                  translatableString: translatableString,
                });
              }
            }
          }
          // We may have collected several replacement candidates for this text
          // node. For example the string "Drupal is awesome." could have a
          // shorter replacement string like "Drupal" somewhere else on the
          // page. Ensure we are replacing the best/longest string possible of
          // the replacement candidates.
          if (replacementCandidate.length > 0) {
            let bestReplacement = replacementCandidate.reduce(function (a, b) {
              return a.replacement.length > b.replacement.length ? a : b;
            });
            bestReplacement.node.innerHTML = bestReplacement.node.innerHTML.replace(bestReplacement.escapedHtmlEntitiesSearch, bestReplacement.replacement);
          }
          replacementCandidate = [];
        } else {
          // If this is not a text node, see if it contains text nodes.
          Drupal.behaviors.stringLocationVariant.findAndReplace(child);
        }
      });
    },
  };
})(jQuery, Drupal, once);

/**
 * Translatable String Web Component.
 *
 * A web component to highlight all translatable strings and load an edit
 * text field when clicked.
 */
class translatableString extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({mode: 'open'});
    var self = this;

    // Create the elements which comprise the web component contents.
    const container = document.createElement('span');
    container.setAttribute('class', 'translated-string');
    container.innerHTML = this.innerHTML;
    const style = document.createElement('style');
    style.textContent = '.translated-string, .translated-string p { background-color: #feff4f !important; color: #000 !important; }' +
      '.string-location-variant-translate-string, .string-location-variant-translate-string * {display: inline-block;}' +
      '.translated-string input, .translated-string textarea {font-size: 14px}' +
      '.translated-string form {background-color: rgb(228, 228, 228); border: 1px solid black; padding: .25em; }' +
      '[data-drupal-selector="edit-queue"] {background-color: transparent; border: none; font-size: 1.8rem; padding-top: 0; padding-bottom: 0;}' +
      '.cancel-translation {font-size: 1.8rem;}' +
      '.cancel-translation:hover, [data-drupal-selector="edit-submit"]:hover, [data-drupal-selector="edit-queue"]:hover {cursor: pointer;}'
    ;
    this.shadowRoot.append(container, style);

    // A translatable string was clicked.
    this.clickHandler = function (e) {
      // Allow the form submit button to operate normally.
      if (e && e.target.parentElement && e.target.parentElement.attributes && e.target.parentElement.attributes['data-drupal-selector'] && e.target.parentElement.attributes['data-drupal-selector'].nodeValue === 'edit-actions') {
        return;
      }
      if (e) {
        e.preventDefault();
      }

      // Close any already open web components. We only allow editing one at a time.
      self.closeWebComponents();

      // Replace the translatable string with a form if it hasn't been already.
      const translatableString = this.shadowRoot.children[0];
      if (translatableString && !translatableString.dataset.form) {
        const ajax = Drupal.ajax({
          url: '/translate-string?destination=' + window.location.pathname,
          translatableString: translatableString,
          type: 'POST',
          progress: true,
          submit: {
            translation: self.dataset.translation.replace(/&apos;/g, "\'"),
            source: self.dataset.source.replace(/&apos;/g, "\'"),
            context: self.dataset.context,
            args: self.dataset.args,
            language: drupalSettings.language,
          },
        });
        ajax.commands.insert = function (ajax, response) {
          // Stash the translatable string as a data attribute when inserting
          // the translation form.
          ajax.translatableString.dataset.original = ajax.translatableString.innerHTML;
          ajax.translatableString.innerHTML = response.data;
          const cancel = document.createElement('span');
          cancel.setAttribute('class', 'cancel-translation')
          cancel.innerHTML = '&#x2715;';
          ajax.translatableString.firstElementChild.lastElementChild.append(cancel);
          ajax.translatableString.dataset.form = false;
          self.dataset.active = 'true';

          // Cancel button click handler.
          cancel.onclick = function (e) {
            self.closeWebComponent();
          };
        };
        ajax.execute();
      }
    }

    this.closeWebComponent = function(e = null) {
      // Replace the form with the original translated string.
      const translatableString = this.shadowRoot.childNodes[0];
      if (this.dataset.active === 'true') {
        translatableString.innerHTML = translatableString.dataset.original;
        delete(translatableString.dataset.form);
        if (e) {
          e.preventDefault();
          e.stopPropagation();
        }
        translatableString.onclick = function (e) {
          self.clickHandler(e);
        };
        this.dataset.active === 'false';
      }
    }

    this.closeWebComponents = function() {
      let webComponents = document.getElementsByTagName('translatable-string');
      for (let webComponent of webComponents) {
        webComponent.closeWebComponent();
      }
    }

    // Translatable string click handler.
    container.onclick = function (e) {
      self.clickHandler(e);
    };
  }
}

customElements.define('translatable-string', translatableString);
