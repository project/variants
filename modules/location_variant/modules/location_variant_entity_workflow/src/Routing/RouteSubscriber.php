<?php

namespace Drupal\location_variant_entity_workflow\Routing;

use Drupal\variants\VariantHandler;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\entity_workflow\EntityWorkflowInfo;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Subscriber for Entity Workflow Content routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityWorkflowInfo $entityWorkflowInfo,
    protected VariantHandler $variantHandler,
  ) {}

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      $workflows_info = $this->entityWorkflowInfo->getWorkflowsInfoForEntityType($entity_type_id);
      if (isset($workflows_info['content']) && $this->variantHandler->hasEnabledVariations($entity_type)) {
        // Require an active workspace before localizing an entity.
        $localization_routes = [
          "entity.$entity_type_id.content_translation_overview",
          "entity.$entity_type_id.location.add",
          "entity.$entity_type_id.localization.add",
          "entity.$entity_type_id.localization.edit",
          "entity.$entity_type_id.translation.add",
          "entity.$entity_type_id.translation.edit",
        ];
        foreach ($localization_routes as $route_name) {
          $route = $collection->get($route_name);
          $route->setOption('_entity_workflow_content.require_workspace', TRUE);
          $route->setOption('_entity_workflow_content.entity_type_id', $entity_type_id);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    // Ensure that we run after Entityqueue's route subscriber so that we can
    // handle those routes as well.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -215];
    return $events;
  }

}
