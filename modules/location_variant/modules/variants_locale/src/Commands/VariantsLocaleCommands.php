<?php

namespace Drupal\variants_locale\Commands;

use Drupal\variants_locale\JsTranslations;
use Drush\Commands\DrushCommands;

class VariantsLocaleCommands extends DrushCommands {

  /**
   * @var \Drupal\variants_locale\JsTranslations
   */
  private JsTranslations $jsTranslations;

  public function __construct(JsTranslations $js_translations) {
    $this->jsTranslations = $js_translations;
  }


  /**
   * Generate JS translation files for window.drupalTranslations
   *
   * @usage variants_locale-generateTranslationFiles
   *   Generate JS translation files for window.drupalTranslations
   *
   * @command variants_locale:generateTranslationFiles
   * @aliases gtf
   */
  public function generateTranslationFiles() {
    $file_names = $this->jsTranslations->generateFiles();
    $rows = [];
    foreach ($file_names as $file_name) {
      $rows[] = [$file_name];
    }
    $this->io()->table(['Generated files'], $rows);
  }

}
