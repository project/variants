<?php

namespace Drupal\variants_locale\Form;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\location_variant\LocationHandler;
use Drupal\variants_locale\JsTranslations;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Variants locale form.
 */
class JsTranslationsForm extends FormBase {


  /**
   * @var \Drupal\variants_locale\JsTranslations
   */
  private JsTranslations $jsTranslations;

  public static function create(ContainerInterface $container) {
    return new static($container->get('variants_locale.translations'));
  }

  public function __construct(JsTranslations $js_translations) {
    $this->jsTranslations = $js_translations;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'variants_locale_js_translations';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message']['#markup'] = $this->t('Generates a JS file of all strings for each language in the system. This populates the JS window.drupalTranslations variable for Drupal.t().');

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $languages = [];
    foreach (\Drupal::languageManager()->getLanguages() as $language) {
      $langcode = $language->getId();
      if (LocationHandler::parseCode($langcode)['type'] === 'langcode') {
        $languages[$langcode] = $language;
      }
    }
    $operations = [];
    foreach (array_chunk($languages, '10') as $chunked_languages) {
      $operations[] = [
        [static::class, 'processGenerateFilesBatch'],
        [$chunked_languages],
      ];
    }
    batch_set([
      'operations' => $operations,
      'title' => t('Generating JS translation files...'),
      'finished' => [static::class, 'finishedGenerateFilesBatch'],
    ]);
  }

  public static function processGenerateFilesBatch($languages, &$context) {
    if (empty($context['results']['file_names'])) {
      $context['results']['file_names'] = [];
    }
    $file_names = \Drupal::service('variants_locale.translations')->generateFiles($languages);
    $context['results']['file_names'] = array_merge($context['results']['file_names'], $file_names);
  }

  public static function finishedGenerateFilesBatch($success, $results, $operations) {
    $list = [
      '#theme' => 'item_list',
      '#items' => $results['file_names'],
    ];
    \Drupal::messenger()->addStatus(t("Generated files: <br>@file_names", [
      '@file_names' => \Drupal::service('renderer')->render($list),
    ]));
  }

}
