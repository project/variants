<?php

namespace Drupal\variants_locale;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class JsTranslations {

  private EntityRepositoryInterface $entityRepository;
  private LanguageManagerInterface $languageManager;
  private EntityTypeManagerInterface $entityTypeManager;
  private FileSystemInterface $fileSystem;
  private ModuleHandlerInterface $moduleHandler;

  public function __construct(EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, ModuleHandlerInterface $module_handler) {
    $this->entityRepository = $entity_repository;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
  }

  public function generateFiles($languages = NULL) {
    // Run outside of a workspace.
    if ($this->moduleHandler->moduleExists('workspaces') && ($workspace_manager = \Drupal::service('workspaces.manager')) && $workspace_manager->getActiveWorkspace() !== FALSE) {
      return $workspace_manager->executeOutsideWorkspace(function () use ($languages) {
        return $this->generator($languages);
      });
    }
    else {
      return $this->generator($languages);
    }
  }

  protected function generator($languages = NULL) {
    $translations = [];

    if (empty($languages)) {
      $languages = $this->languageManager->getLanguages();
    }

    $strings = $this->entityTypeManager->getStorage('string')->loadMultiple();
    // Prepare a list of drupalTranslation values for each language.
    foreach ($languages as $language) {
      $langcode = $language->getId();
      if (LocationHandler::parseCode($langcode)['type'] === 'langcode') {
        foreach ($strings as $string) {
          $translation = $this->entityRepository->getTranslationFromContext($string, $langcode);
          $translations[$langcode]['strings'][$translation->get('context')->value][$translation->get('source')->value] = $translation->label();
        }
      }
    }
    // Write a JS translation file for each language.
    $filenames = [];
    foreach ($translations as $langcode => $data) {
      $json_data = 'window.drupalTranslations = ' . Json::encode($data) . ';';
      $dir = 'public://languages';
      $dest = "$dir/$langcode.js";
      $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
      if (file_exists($dest)) {
        $this->fileSystem->delete($dest);
      }

      $filenames[] = $this->fileSystem->saveData($json_data, $dest);
    }
    return $filenames;
  }

}
