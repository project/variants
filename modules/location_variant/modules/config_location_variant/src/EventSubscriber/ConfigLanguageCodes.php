<?php

namespace Drupal\config_location_variant\EventSubscriber;

use Drupal\Core\Language\Language;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\variants\Event\LanguagesEvent;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Languages event subscriber.
 */
class ConfigLanguageCodes implements EventSubscriberInterface {

  private Connection $connection;
  private ImmutableConfig $settings;
  private LocationHandler $locationHandler;

  public function __construct(LocationHandler $location_handler, Connection $connection, ConfigFactoryInterface $config) {
    $this->locationHandler = $location_handler;
    $this->connection = $connection;
    $this->settings = $config->get('variants.settings');
  }

  /**
   * On get languages.
   *
   * @param \Drupal\variants\Event\LanguagesEvent $event
   *   The languages event.
   */
  public function onGetLanguages(LanguagesEvent $event) {
    if (!$this->connection->schema()->tableExists('variant_revision__locations')) {
      return;
    }
    $locations = $this->locationHandler->getLocations();

    // Config doesn't use localization. Add a language for each langcode without
    // the location.
    foreach ($locations as $location_id => $location) {
      foreach ($location->getLanguages() as $langcode => $values) {
        if (is_null($event->getLanguage($langcode))) {
          $variation_language = $this->newLanguage($langcode, $values['label'], $values['direction'] ?? 'ltr');
          $event->addLanguage($variation_language);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LanguagesEvent::class => ['onGetLanguages', 100],
    ];
  }

  /**
   * New language.
   *
   * @param string $id
   *   The language ID.
   * @param string $name
   *   The language name.
   * @param string $direction
   *   The langauge dircetion.
   *
   * @return \Drupal\Core\Language\Language
   *   A new language.
   */
  private function newLanguage(string $id, string $name, string $direction = 'ltr') {
    return new Language([
      'id' => $id,
      'name' => $name,
      'direction' => $direction,
    ]);
  }

}
