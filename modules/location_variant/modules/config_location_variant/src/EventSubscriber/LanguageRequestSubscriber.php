<?php

namespace Drupal\config_location_variant\EventSubscriber;

use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\variants\VariantsLanguageManager;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sets the $request property on the language manager.
 */
class LanguageRequestSubscriber implements EventSubscriberInterface {

  protected LanguageManagerInterface $languageManager;
  protected AccountInterface $currentUser;

  public function __construct(LanguageManagerInterface $language_manager, AccountInterface $current_user) {
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Initializes the language manager at the beginning of the request.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function onKernelRequestLanguage(RequestEvent $event) {
    if ($event->isMainRequest()) {
      $this->setLanguageOverrides();
    }
  }

  /**
   * Initializes config overrides whenever the service container is rebuilt.
   */
  public function onContainerInitializeSubrequestFinished() {
    $this->setLanguageOverrides();
  }

  /**
   * Sets the language for config overrides on the language manager.
   */
  private function setLanguageOverrides() {
    if ($this->languageManager instanceof VariantsLanguageManager) {
      $language = $this->languageManager->getCurrentLanguage();
      $this->languageManager->setConfigOverrideLanguage($language);
    }
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestLanguage', 255];
    $events[DrupalKernelInterface::CONTAINER_INITIALIZE_SUBREQUEST_FINISHED][] = ['onContainerInitializeSubrequestFinished', 255];

    return $events;
  }

}
