<?php

/**
 * @file
 * Provides views data for location_variant.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function location_variant_views_data_alter(&$data) {
  \Drupal::service('location_variant.view_handler')->viewsDataAlter($data);
}
