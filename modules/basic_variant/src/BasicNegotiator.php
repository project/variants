<?php

namespace Drupal\basic_variant;

use Drupal\variants\NegotiatorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Basic Negotiator.
 */
class BasicNegotiator implements NegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request):string {
    $parts = \explode('/', \trim($path, '/'));
    $prefix = \array_shift($parts);
    // Search prefix within added languages.
    $languages = \Drupal::languageManager()->getLanguages();
    if (\array_key_exists($prefix, $languages)) {
      // Rebuild $path with the language removed.
      $path = '/' . \implode('/', $parts);
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL):string {
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentLanguage(Request $request = NULL) {
    $langcode = NULL;

    if ($request) {
      $languages = \Drupal::languageManager()->getLanguages();
      $request_path = \urldecode(\trim($request->getPathInfo(), '/'));
      $path_args = \explode('/', $request_path);
      $prefix = \array_shift($path_args);

      // Search prefix within added languages.
      if (!empty($languages[$prefix])) {
        return $languages[$prefix];
      }
    }

    return $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackCandidates(array &$candidates = [], array $context = []) {
    $pieces = \explode('-', $context['langcode']);
    if ($pieces && $pieces[0] === 'v') {
      $url_prefix = \sprintf('variant-%s-%s', $context['data']->id(), $pieces[1]);
      $candidates = [$url_prefix => $url_prefix] + $candidates;
    }
    if (!empty($context['langcode'])) {
      $candidates = [$context['langcode'] => $context['langcode']] + $candidates;
    }
  }

}
