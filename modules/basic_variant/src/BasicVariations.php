<?php

namespace Drupal\basic_variant;

use Drupal\Core\Database\Database;
use Drupal\variants\Entity\Variant;
use Drupal\variants\VariantHandler;
use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityInterface;

class BasicVariations {

  public VariantHandler $variantHandler;

  public function __construct(VariantHandler $variant_handler) {
    $this->variantHandler = $variant_handler;
  }

  /**
   * Track.
   *
   * Handle variation tracking via the Variant entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to track.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function insert(EntityInterface $entity) {
    if ($this->variantHandler->hasEnabledVariations($entity)) {
      $this->createVariant($entity);
      $this->variantHandler->prepareVariation($entity, $entity->language()->getId(), 'variant-1-1');
    }
  }

  /**
   * Create variant.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to vary.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createVariant(EntityInterface $entity) {
    $variant = Variant::create([
      'entity_id' => $entity->id(),
      'type' => $entity->getEntityTypeId(),
    ]);
    $variant->save();

    $variants = $entity->get('variants')->getValue();
    $variants[] = ['target_id' => $variant->id()];
    $entity->set('variants', $variants);

    return $variant;
  }

  /**
   * Add example variations.
   *
   * @param string $entity_id
   *   The entity ID of the entity to vary.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addExampleVariations(string $entity_id) {
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $vid = $storage->getLatestRevisionId($entity_id);
    $entity = $storage->loadRevision($vid);
    foreach ([
        "variant-$entity_id-2",
        "variant-$entity_id-3",
        "variant-$entity_id-4",
        "variant-$entity_id-5",
      ] as $target) {
      $this->addVariation($entity, $target);
    }
  }

  /**
   * Add variation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to vary.
   * @param string $target
   *   The target variation code.
   * @param string $source
   *   The source variation code.
   */
  public function addVariation(EntityInterface $entity, string $target, string $source = 'variant-1-1') {
    $this->variantHandler->prepareVariation($entity, $source, $target);
    $entity->setNewRevision();
    $entity = $entity->getTranslation($target);
    $type = $entity->getEntityType();
    $entity->set($type->getKey('label'), $target . ' ' . (new Random())->name());
    $entity->save();
    $workspace = $entity->get('workspace')->target_id;
    if ($workspace) {
      Database::getConnection()
        ->update('workspace_association')
        ->fields(['target_entity_revision_id' => $entity->vid->value])
        ->condition('workspace', $workspace)
        ->condition('target_entity_type_id', 'node')
        ->condition('target_entity_id', $entity->id())
        ->execute();
    }
  }

}
