<?php

namespace Drupal\basic_variant\EventSubscriber;

use Drupal\Core\Language\Language;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\variants\Event\LanguagesEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Languages event subscriber.
 */
class VariationLanguageCodes implements EventSubscriberInterface {

  private Connection $connection;
  private ImmutableConfig $settings;
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a MultipleLocationLangcodes object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config, EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->settings = $config->get('variants.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Drupal\variants\Event\LanguagesEvent $event
   *   The languages event.
   */
  public function onGetLanguages(LanguagesEvent $event) {
    foreach ([
               "variant-1-1",
               "variant-1-2",
               "variant-1-3",
               "variant-1-4",
               "variant-1-5",
               "variant-1-6",
               'v-1',
               'v-2',
               'v-3',
               'v-4',
               'v-5',
               'v-6',
               'v-7',
               'v-8',
             ] as $variation_code) {
      $variation_language = new Language([
        'id' => $variation_code,
        'name' => $variation_code,
      ]);
      $event->addLanguage($variation_language);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LanguagesEvent::class => ['onGetLanguages'],
    ];
  }

}
