# Basic Variant

This module is for demonstration purposes only. I think it achieves three things.
- I wanted to codify what I've learned so others can step through the variant pipeline in its simplest form.
- We can boil down the actual requirements for the process and make sure that it's optimized. Once we layer locations and languages on top we still need to satisfy those requirements, but they get smeared around to different places depending on the workflow. So it's good to have a reference of what the the requirements are. It's good to have that skeleton version.
- And lastly, we can test the variant system by itself, so we can more easily diagnose a regression.

#### To use
Create _one_ node on a _clean install_ then run:
```php
\Drupal::service('basic_variation')->addExampleVariations(1);
```
You should be able to go to URLs like `/node/1`, `/variant-1-3/node/1` and `/v-4/node/1`.
