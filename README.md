# Variants
A generic variants system and a foundation for many types of entity variations. It uses an entities langcode column as a handle for variation references so that language managers can point to the right variation to display.

# Location Variants

`location_variants` aims to be a drop in replacement for core's `content_translation` module. Instead of just having translations for an entity you can have many localizations, translations, and localized translations.

## Installation
`location_variants` requires a separate custom module that needs to subscribe to the `LocationEvent` and provide a hierarchy of Locations and the languages they support. e.g.

 ```yaml
global:
  label: Global
  languages:
    en:
      label: English
US:
  label: United States
  parent: global
  languages:
    en:
      label: English
austin:
  label: Austin
  parent: US
  languages:
    en:
      label: English
CA:
  label: Canada
  parent: global
  languages:
    en:
      label: English
    fr:
      label: French Canadian
 ```
Then install `location_variants` as any other Drupal module. Enabling Localizations on an entity changes the entity's langcode, so you should make a back up of your database before proceeding. To enable Localization on an entity navigate Configuration > Workflow > Variants and check one of the entity types. You should now have a Localization local task on that entity where you
can add Localizations and Translations for the langauges that location supports. When viewing a path such as `/node/1` you can get the most specific location and language by prefixing your preference to the path like `en-US/node/1` or `en-austin/node/1` or `fr-CA/node/1`. There is no location selector included in the module at this time.

## Views
Add the `Variations` views relationship to apply location and langcode fallbacks to the list of entities. A `/langcode-location_code/` prefix must be in the URL for anything to show.

#### Glossary
- **Translation** A language variation.
- **Localization** A location or locations variation.
