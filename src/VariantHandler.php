<?php

namespace Drupal\variants;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Variant handler.
 */
class VariantHandler {

  private ConfigFactoryInterface $config;

  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config;
  }

  /**
   * Populates target values with the source values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being translated.
   * @param string $source_variation_code
   *   The language to be used as source.
   * @param string $target_variation_code
   *   The language to be used as target.
   */
  public function prepareVariation(ContentEntityInterface $entity, string $source_variation_code, string $target_variation_code) {
    $source_variation = $entity->getTranslation($source_variation_code);
    $target_variation = $entity->addTranslation($target_variation_code, $source_variation->toArray());

    // Make sure we do not inherit the affected status from the source values.
    if ($entity->getEntityType()->isRevisionable()) {
      $target_variation->setRevisionTranslationAffected(NULL);
    }
  }

  /**
   * Has enabled variations.
   *
   * @param \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityTypeInterface $entity
   *   The entity to interrogate.
   *
   * @return bool
   *   Whether or not the entity has variation support enabled.
   */
  public function hasEnabledVariations($entity): bool {
    if ($entity instanceof EntityInterface) {
      $entity_type = $entity->getEntityTypeId();
    }
    if ($entity instanceof EntityTypeInterface) {
      $entity_type = $entity->id();
    }
    $config = $this->config->get('variants.settings');
    if (isset($entity_type) && $enabled_entity_types = $config->get('entity_types')) {
      return \in_array($entity_type, array_keys($enabled_entity_types));
    }
    return FALSE;
  }

}
