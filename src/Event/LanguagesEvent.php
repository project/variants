<?php

namespace Drupal\variants\Event;

use Drupal\Core\Language\Language;
use Drupal\Component\EventDispatcher\Event;

/**
 * The language manager get languages event.
 */
class LanguagesEvent extends Event {

  protected array $languages = [];

  public function __construct(array $languages) {
    $this->languages = $languages;
  }

  public function getLanguages(): array {
    return $this->languages;
  }

  public function getLanguage(string $language): ?Language {
    return $this->languages[$language] ?? NULL;
  }

  public function addLanguage(Language $language) {
    $this->languages[$language->getId()] = $language;
  }

}
