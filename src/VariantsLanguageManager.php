<?php

namespace Drupal\variants;

use Drupal\Core\Language\Language;
use Drupal\variants\Event\LanguagesEvent;
use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\language\ConfigurableLanguageManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\language\Config\LanguageConfigFactoryOverrideInterface;

/**
 * Variants language manager.
 */
class VariantsLanguageManager extends ConfigurableLanguageManager {
  private NegotiationManager $negotiationManager;
  private EventDispatcherInterface $dispatcher;

  public function __construct(LanguageDefault $default_language, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, LanguageConfigFactoryOverrideInterface $config_override, RequestStack $request_stack, NegotiationManager $negotiation_manager, EventDispatcherInterface $dispatcher) {
    parent::__construct($default_language, $config_factory, $module_handler, $config_override, $request_stack);
    $this->dispatcher = $dispatcher;
    $this->negotiationManager = $negotiation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguages($flags = LanguageInterface::STATE_CONFIGURABLE) {
    if (!isset($this->languages[$flags])) {

      // Default languages.
      $default = $this->getDefaultLanguage();
      $languages = [$default->getId() => $default];
      $languages += $this->getDefaultLockedLanguages($default->getWeight());

      // More languages.
      $event = $this->dispatcher->dispatch(new LanguagesEvent($languages));
      $languages = $event->getLanguages();

      Language::sort($languages);
      $this->languages[$flags] = $this->filterLanguages($languages, $flags);
    }

    return $this->languages[$flags];
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentLanguage($type = LanguageInterface::TYPE_INTERFACE): LanguageInterface {
    if (!isset($this->negotiatedLanguages[$type])) {
      // Ensure we have a valid value for this language type.
      $this->negotiatedLanguages[$type] = $this->getDefaultLanguage();

      if (!isset($this->initializing[$type])) {
        $this->initializing[$type] = TRUE;
        $this->negotiatedLanguages[$type] = $this->negotiationManager->getCurrentLanguage($type);
        unset($this->initializing[$type]);
      }
      // If the current interface language needs to be retrieved during
      // initialization we return the system language. This way string
      // translation calls happening during initialization will return the
      // original strings which can be translated by calling them again
      // afterwards. This can happen for instance while parsing negotiation
      // method definitions.
      elseif ($type == LanguageInterface::TYPE_INTERFACE) {
        return new Language(['id' => LanguageInterface::LANGCODE_SYSTEM]);
      }
    }

    return $this->negotiatedLanguages[$type];
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackCandidates(array $context = []) {
    $candidates = \array_keys($this->getLanguages());
    $candidates[] = LanguageInterface::LANGCODE_NOT_SPECIFIED;
    $candidates = \array_combine($candidates, $candidates);
    $this->negotiationManager->getFallbackCandidates($candidates, $context);
    return $candidates;
  }

  /**
   * {@inheritdoc}
   */
  public function isMultilingual() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigOverrideLanguage(LanguageInterface $language = NULL) {
    if ($this->configFactoryOverride) {
      $this->configFactoryOverride->setLanguage($language);
    }
    return $this;
  }

}
