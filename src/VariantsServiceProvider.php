<?php

namespace Drupal\variants;

use Symfony\Component\DependencyInjection\Reference;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Defines a service provider for the Variants module.
 */
class VariantsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('language_manager');
    $definition->setClass('Drupal\variants\VariantsLanguageManager')
      ->addArgument(new Reference('negotiation_manager'))
      ->addArgument(new Reference('event_dispatcher'));

    if ($container->hasDefinition('config_location_variant.config_factory_override')) {
      $definition->addArgument(new Reference('config_location_variant.config_factory_override'));
    }
  }

}
