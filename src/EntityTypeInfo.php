<?php

namespace Drupal\variants;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

class EntityTypeInfo {

  private VariantHandler $variantHandler;
  private ConfigFactoryInterface $config;

  public function __construct(ConfigFactoryInterface $config, VariantHandler $variant_handler) {
    $this->config = $config;
    $this->variantHandler = $variant_handler;
  }

  /**
   * Entity base field info.
   *
   * Adds variant field to enabled entities.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by field name.
   *
   * @see hook_entity_base_field_info()
   */
  public function entityBaseFieldInfo(EntityTypeInterface $entity_type) {
    if ($this->variantHandler->hasEnabledVariations($entity_type)) {
      $fields['variants'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Variants'))
        ->setSetting('target_type', 'variant')
        ->setInternal(TRUE)
        ->setTranslatable(FALSE)
        ->setCardinality($this->config->get('variants.settings')->get('variant_limit'));

      return $fields;
    }
  }

}
