<?php

namespace Drupal\variants\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Variants settings for this site.
 */
class VariantsSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'variant_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('variants.settings')->getRawData();
    $form['variant_limit'] = [
      '#title' => $this->t('Variant Limit'),
      '#description' => $this->t('The number of variants an entity can have.'),
      '#type' => 'textfield',
      '#size' => 5,
      '#default_value' => $config['variant_limit'] ?? 10,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('variants.settings');
    $config
      ->set('variant_limit', $form_state->getValue('variant_limit'))
      ->save();
  }

}
