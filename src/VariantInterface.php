<?php

namespace Drupal\variants;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a variant entity type.
 */
interface VariantInterface extends ContentEntityInterface {}
