<?php

namespace Drupal\variants\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Variant type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "variant_type",
 *   label = @Translation("Variant type"),
 *   label_collection = @Translation("Variant types"),
 *   label_singular = @Translation("variant type"),
 *   label_plural = @Translation("variants types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count variants type",
 *     plural = "@count variants types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\variants\Form\VariantTypeForm",
 *       "edit" = "Drupal\variants\Form\VariantTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\variants\VariantTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer variant types",
 *   bundle_of = "variant",
 *   config_prefix = "variant_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/variant_types/add",
 *     "edit-form" = "/admin/structure/variant_types/manage/{variant_type}",
 *     "delete-form" = "/admin/structure/variant_types/manage/{variant_type}/delete",
 *     "collection" = "/admin/structure/variant_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class VariantType extends ConfigEntityBundleBase {

  /**
   * The machine name of this variant type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the variant type.
   *
   * @var string
   */
  protected $label;

}
