<?php

namespace Drupal\variants\Entity;

use Drupal\variants\VariantInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\location_variant\LocationHandler;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;

/**
 * Defines the variant entity class.
 *
 * @ContentEntityType(
 *   id = "variant",
 *   label = @Translation("Variant"),
 *   label_collection = @Translation("Variants"),
 *   label_singular = @Translation("variant"),
 *   label_plural = @Translation("variants"),
 *   label_count = @PluralTranslation(
 *     singular = "@count variant",
 *     plural = "@count variants",
 *   ),
 *   bundle_label = @Translation("Variant type"),
 *   handlers = {
 *     "list_builder" = "Drupal\variants\VariantListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\variants\Form\VariantForm",
 *       "edit" = "Drupal\variants\Form\VariantForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\variants\Routing\VariantHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "variant",
 *   revision_table = "variant_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer variant types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/variant",
 *     "add-form" = "/variant/add/{variant_type}",
 *     "add-page" = "/variant/add",
 *     "canonical" = "/variant/{variant}",
 *     "edit-form" = "/variant/{variant}",
 *     "delete-form" = "/variant/{variant}/delete",
 *   },
 *   bundle_entity_type = "variant_type",
 *   field_ui_base_route = "entity.variant_type.edit_form",
 * )
 */
class Variant extends RevisionableContentEntityBase implements VariantInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Get entity.
   *
   * Gets the entity that this variant applies to.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getEntity(): EntityInterface {
    return $this->entityTypeManager()->getStorage($this->type->value)->load($this->entity_id->value);
  }

  /**
   * Get variation.
   *
   * Gets a variation (translation) of this variant.
   *
   * @param string $variation_code
   *   The variant code (as it is in the database).
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The variation (translation).
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getVariation(string $variation_code = LocationHandler::DEFAULT_LANGUAGE) {
    $entity = $this->getEntity();
    if ($entity->hasTranslation($variation_code)) {
      return $entity->getTranslation($variation_code);
    }
    return NULL;
  }

  /**
   * Get the supported langcodes for this location reference.
   *
   * @return string[]
   *   An array of supported langcodes.
   */
  public function getSupportedLangcodes(): array {
    $supported_langcodes = [];
    foreach ($this->getLocations() as $location) {
      $supported_langcodes += $this->getHandler()->getSupportedLangcodes($location);
    }
    return $supported_langcodes;
  }

  /**
   * Get locations.
   *
   * @return \Drupal\location_variant\Location[]
   *   An array of locations for this variant.
   */
  public function getLocations(): array {
    $locations = [];
    foreach ($this->get('locations')->getValue() as $location_id) {
      $location = $this->getHandler()->getLocation($location_id['value']);
      $locations[$location->getId()] = $location;
    }
    return $locations;
  }

  /**
   * Get the location IDs for this Variant.
   *
   * @return array
   *   An array of location IDs.
   */
  public function getLocationIds() {
    $locations = [];
    foreach ($this->get('locations')->getValue() as $location_id) {
      $locations[] = $location_id['value'];
    }
    return $locations;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(\t('Entity ID'))
      ->setDescription(\t('The ID of the entity of which this location reference applies.'))
      ->setRequired(TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(\t('Entity Type'))
      ->setDescription(\t('The type of the referenced entity.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH)
      ->setRequired(TRUE);

    return $fields;
  }

  /**
   * Get the location handler service.
   */
  private function getHandler(): LocationHandler {
    return \Drupal::service('location_variant.location_handler');
  }

}
