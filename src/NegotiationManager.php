<?php

namespace Drupal\variants;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Language\LanguageInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;

/**
 * The negotiation manager.
 */
class NegotiationManager implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * An unsorted array of arrays of active negotiators.
   *
   * An associative array. The keys are integers that indicate priority. Values
   * are arrays of NegotiatorInterface objects.
   *
   * @var \Drupal\variants\NegotiatorInterface[][]
   *
   * @see \Drupal\variants\NegotiationManager::addNegotiator()
   * @see \Drupal\variants\NegotiationManager::sortNegotiators()
   */
  protected $negotiators = [];

  /**
   * An array of negotiators, sorted by priority.
   *
   * If this is NULL a rebuild will be triggered.
   *
   * @var \Drupal\variants\NegotiatorInterface[][]
   *
   * @see \Drupal\variants\NegotiationManager::addTranslator()
   * @see \Drupal\variants\NegotiationManager::sortNegotiators()
   */
  protected $sortedNegotiators = NULL;
  private ?Request $request;

  public function __construct(RequestStack $request_stack) {
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * Appends a negotiation system to the negotiation chain.
   *
   * @param \Drupal\variants\NegotiatorInterface $negotiator
   *   The negotiation interface to be appended to the negotiation chain.
   * @param int $priority
   *   The priority of the negotiator being added.
   *
   * @return $this
   */
  public function addNegotiator(NegotiatorInterface $negotiator, $priority = 0) {
    $this->negotiators[$priority][] = $negotiator;
    // Reset sorted negotiators property to trigger rebuild.
    $this->sortedNegotiators = NULL;
    return $this;
  }

  /**
   * Sorts negotiators according to priority.
   *
   * @return \Drupal\variants\NegotiatorInterface[]
   *   A sorted array of negotiator objects.
   */
  protected function sortNegotiators() {
    if ($this->sortedNegotiators === NULL) {
      $sorted = [];
      \krsort($this->negotiators);

      foreach ($this->negotiators as $negotiators) {
        $sorted = \array_merge($sorted, $negotiators);
      }
      $this->sortedNegotiators = $sorted;
    }
    return $this->sortedNegotiators;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $this->sortNegotiators();
    foreach ($this->sortedNegotiators as $negotiator) {
      $path = $negotiator->processInbound($path, $request);
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    $this->sortNegotiators();
    foreach ($this->sortedNegotiators as $negotiator) {
      $path = $negotiator->processOutbound($path, $options, $request, $bubbleable_metadata);
      if ($path !== FALSE) {
        return $path;
      }
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentLanguage($type = LanguageInterface::TYPE_INTERFACE) {
    $this->sortNegotiators();
    $language = NULL;
    foreach ($this->sortedNegotiators as $negotiator) {
      $language = $negotiator->getCurrentLanguage($this->request);
      if ($language) {
        // Since objects are references, we should pass a clone.
        $language = clone $language;
      }
    }
    if (!$language) {
      $language = \Drupal::languageManager()->getDefaultLanguage();
    }

    return $language;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackCandidates(array &$candidates = [], array $context = []) {
    $this->sortNegotiators();
    foreach ($this->sortedNegotiators as $negotiator) {
      $negotiator->getFallbackCandidates($candidates, $context);
    }
  }

}
