<?php

namespace Drupal\variants\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Install variant field.
 */
class InstallVariantField implements EventSubscriberInterface {

  private Connection $connection;
  private ImmutableConfig $settings;
  private EntityTypeManagerInterface $entityTypeManager;
  private EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager;

  public function __construct(Connection $connection, ConfigFactoryInterface $config, EntityTypeManagerInterface $entity_type_manager, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager) {
    $this->connection = $connection;
    $this->settings = $config->get('variants.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
  }

  public function onConfigSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    if ($config->getName() !== 'variants.settings') {
      return;
    }
    $original = $config->getOriginal()['entity_types'] ?? [];
    $current = $config->getRawData()['entity_types'] ?? [];

    $enabled_entity_types = \array_diff(array_keys($current), array_keys($original));
    foreach ($enabled_entity_types as $enabled_entity_type) {
      $this->installVariantsReferenceField($enabled_entity_type);
    }

    $disabled_entity_types = \array_diff(array_keys($original), array_keys($current));
    foreach ($disabled_entity_types as $disabled_entity_type) {
      $this->uninstallVariantsReferenceField($disabled_entity_type);
    }

    if ($original || $current) {
      drupal_flush_all_caches();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => ['onConfigSave', 200],
    ];
  }

  protected function installVariantsReferenceField(string $enabled_entity_type) {
    $entity_type = $this->entityTypeManager->getStorage($enabled_entity_type)->getEntityType();
    $table = $entity_type->getBaseTable();
    if (!$this->connection->schema()->tableExists("{$table}__variants")) {
      $storage_definition = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Variants'))
        ->setSetting('target_type', 'variant')
        ->setInternal(TRUE)
        ->setTranslatable(FALSE)
        ->setCardinality($this->settings->get('variant_limit'));
      $this->entityDefinitionUpdateManager->installFieldStorageDefinition('variants', $enabled_entity_type, 'entity_workflow', $storage_definition);
    }
  }

  protected function uninstallVariantsReferenceField(string $enabled_entity_type) {
    $entity_type = $this->entityTypeManager->getStorage($enabled_entity_type)->getEntityType();
    $table = $entity_type->getBaseTable();
    if ($this->connection->schema()->tableExists("{$table}__variants")) {
      $storage_definition = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Variants'))
        ->setSetting('target_type', 'variant')
        ->setInternal(TRUE)
        ->setTranslatable(FALSE)
        ->setTargetEntityTypeId($entity_type->id())
        ->setName('variants')
        ->setCardinality($this->settings->get('variant_limit'));
      $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($storage_definition);
    }
  }
}
