<?php

namespace Drupal\variants\EventSubscriber;

use Drupal\wse_config\Event\WseConfigOptOutEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Wse config event subscriber.
 */
class WseConfigSubscriber implements EventSubscriberInterface {

  /**
   * Provides a default list of ignored configs.
   *
   * @param \Drupal\wse_config\Event\WseConfigOptOutEvent $event
   *   The wse config opt out event.
   */
  public function onWseConfigOptOut(WseConfigOptOutEvent $event) {
    $config_names = [
      'core.base_field_override.*',
      'workflows.workflow.*',
      'variants.settings',
      'field.field.*',
    ];
    $event->setIgnored(...$config_names);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events['wse_config.opt_out'][] = 'onWseConfigOptOut';
    return $events;
  }

}
